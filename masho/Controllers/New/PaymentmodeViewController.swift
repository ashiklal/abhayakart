//
//  PaymentmodeViewController.swift
//  masho
//
//  Created by WC46 on 23/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
import Kingfisher
class PaymentmodeViewController: UIViewController{
    var delegate : displayNewVCdelegate! = nil
    var UserID : String = ""
    var guest_id : Int = 0
    var TableID : String = ""
    var order_id : String = ""
    var paymentData : MHPaymentModel!
    @IBOutlet var paymentmode:UITableView!
     var Paymentmodetitles = ["Cash On Delivery","Google Pay","PhonePe/BHIM UPI ","Paytm","Credit/Debit/ATM Card","Net Banking","Wallets"]
    var indexpath: IndexPath = [0, 0]
    override func viewDidLoad() {
        super.viewDidLoad()
         paymentmode.dataSource = self
        paymentmode.delegate = self
        getPaymentMethod()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func ConfirmOrder(_ sender: Any) {
//        if indexpath.section == 0{
//            let vc = StoryBoard.new.instantiateViewController(withIdentifier: "SuccessfullViewController") as! SuccessfullViewController
//            self.navigationController?.pushViewController(vc, animated: false)
//        }else{
//            let vc = StoryBoard.new.instantiateViewController(withIdentifier: "FailureViewController") as! FailureViewController
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
        gotoWeb(index: indexpath.row)
    }
    
    
}
extension PaymentmodeViewController:UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int{
        return 5
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if paymentData != nil{
                return paymentData.paymentoptions.count
            }
            return 0
        }else if section == 1{
            return  1
        }else if section == 2{
            return  1
        }else if section == 3{
            if paymentData != nil {
                return self.paymentData.pricedetails.entryloop.count + 1
            }
            return 0
        }else{
            return  1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if ( indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier:"PaymentmodeTableViewCell1") as! PaymentmodeTableViewCell1
            cell.title.text = paymentData.paymentoptions[indexPath.row].name
            cell.gpayview.kf.setImage(with: URL(string: paymentData.paymentoptions[indexPath.row].image!))
            if indexpath == indexPath{
                cell.Paymentradiobuttonimage.image = UIImage(named:"square-3")
//                cell.backgroundColor = UIColor.lightGray.withAlphaComponent(0.1)
            }else{
                cell.Paymentradiobuttonimage.image = UIImage(named:"square-1")
//                cell.backgroundColor = UIColor.clear
            }
            return cell
        }else if indexPath.section == 1{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewBottomTVC") as! MHReviewBottomTVC
                if self.paymentData != nil{
                cell.descLb.text = self.paymentData.pricedetails.amountpayable.bottomtxt.text
                cell.descLb.textColor = UIColor(hex: "\(self.paymentData.pricedetails.amountpayable.bottomtxt.color!)ff")
                cell.shieldIcon.kf.setImage(with: URL(string: self.paymentData.pricedetails.amountpayable.bottomtxt.icon))
                }
                return cell
        }else if indexPath.section == 2{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewPriceDetailsHeaderTVC") as! MHReviewPriceDetailsHeaderTVC
                if paymentData != nil{
                    cell.TitleLb.text = self.paymentData.pricedetails.amountpayable.text
                }
                return cell
        }else if indexPath.section == 3{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MHReviewPriceDetailsTVC") as! MHReviewPriceDetailsTVC
                DispatchQueue.main.async {
                    if indexPath.row >= self.paymentData.pricedetails.entryloop.count{
                        cell.TitleLb.text = self.paymentData.pricedetails.amountpayable.text
                        cell.PriceLb.text = "\(self.paymentData.pricedetails.amountpayable.amount!)"
                        cell.TitleLb.font = UIFont(name: "Roboto-Medium", size: 14)
                        cell.PriceLb.font = UIFont(name: "Roboto-Medium", size: 14)
                        cell.SeperatorLine.isHidden = false
                            //                    cell.TopConstraint.constant = 30
                    }else{
                        cell.TitleLb.text = self.paymentData.pricedetails.entryloop[indexPath.row].text
                        cell.PriceLb.text = self.paymentData.pricedetails.entryloop[indexPath.row].price
                        if self.paymentData.pricedetails.entryloop[indexPath.row].subtext == nil{
            //                        cell.SubTxtLb.isHidden = true
                        }else{
            //                        cell.SubTxtLb.isHidden = false
                            cell.SubTxtLb.text = self.paymentData.pricedetails.entryloop[indexPath.row].subtext
                        }
                        if self.paymentData.pricedetails.entryloop[indexPath.row].style != nil{
                            cell.backgroundColor = UIColor(hex: self.paymentData.pricedetails.entryloop[indexPath.row].style.background)
                            cell.TitleLb.textColor = UIColor(hex: self.paymentData.pricedetails.entryloop[indexPath.row].style.color)
                            cell.SubTxtLb.textColor = UIColor(hex: self.paymentData.pricedetails.entryloop[indexPath.row].style.color)
                            cell.PriceLb.textColor = UIColor(hex: self.paymentData.pricedetails.entryloop[indexPath.row].style.color)
                        }else{
                            cell.backgroundColor = UIColor.white
                            cell.TitleLb.textColor = UIColor.black
                            cell.SubTxtLb.textColor = UIColor.black
                            cell.PriceLb.textColor = UIColor.black
                        }
                        cell.TitleLb.font = UIFont(name: "Roboto-Regular", size: 13)
                        cell.PriceLb.font = UIFont(name: "Roboto-Regular", size: 13)
                        cell.SeperatorLine.isHidden = true
                            //                    cell.TopConstraint.constant = 0
                    }
                }
                return cell
        }else{
            let cell2 = self.paymentmode.dequeueReusableCell(withIdentifier:"PaymentmodeTableViewCell2",for: indexPath)as!PaymentmodeTableViewCell2
            
            return cell2
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if ( indexPath.section == 0){
            return 55
        }else if indexPath.section == 1{
            return UITableView.automaticDimension
        }else if indexPath.section == 2{
             return UITableView.automaticDimension
        }else if indexPath.section == 3{
            return UITableView.automaticDimension
        }else if indexPath.section == 4{
            return UITableView.automaticDimension
        }else{
            return UITableView.automaticDimension
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if ( indexPath.section == 0){
            indexpath = indexPath
            print(indexpath)
            tableView.reloadData()
        }
        
        
    }
    func gotoWeb(index: Int){
        let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        WebViewVC.WebURL = paymentData.paymentoptions[index].url
        WebViewVC.paymentwebpages = paymentData.paymentwebpages
        self.navigationController!.pushViewController(WebViewVC, animated: true)
    }
}
extension PaymentmodeViewController{
    func getPaymentMethod(){
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        order_id = UserDefaults.standard.value(forKey: "orderid") as? String ?? ""
        let parameters = ["action":"payment",
                          "userId":"\(UserID)",
                          "orderid":order_id,
                          "tableid": TableID] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String: Any]{
                    self.paymentData = MHPaymentModel(fromData: data)
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                self.paymentmode.reloadData()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
