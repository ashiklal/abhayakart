//
//  FailureViewController.swift
//  masho
//
//  Created by WC46 on 23/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
class FailureViewController: UIViewController {
    @IBOutlet weak var mainheadLb:UILabel!
    @IBOutlet weak var subheadLb:UILabel!
    var UserID : String = ""
    var order_id : String = ""
    var TableID : String = ""
    var failureData : MHPaymentSuccessdataModel!
    override func viewDidLoad() {
        super.viewDidLoad()
getFailuredata()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.8) {
//            self.navigationController?.popViewController(animated: false)
//        }
    }
    

}
extension FailureViewController{
    func getFailuredata(){
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        order_id = UserDefaults.standard.value(forKey: "orderid") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        let parameters = ["action":"paymentfailed",
                          "userId":"\(UserID)",
                          "orderid":order_id,
                          "tableid": TableID] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String: Any]{
                    self.failureData = MHPaymentSuccessdataModel(fromData: data)
                }
                
                self.mainheadLb.text = self.failureData.mainhead
                self.subheadLb.text = self.failureData.subhead
                
                let navigationStack = self.navigationController?.viewControllers ?? []
                for vc in navigationStack{
                    if let BaseViewVC = vc as? MHBaseViewVC{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                            self.navigationController?.popToViewController(BaseViewVC, animated: true)
                        }
                    }
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
