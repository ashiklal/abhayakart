//
//  CategoriesModel.swift
//  masho
//
//  Created by Castler on 27/03/21.
//  Copyright © 2021 Appzoc. All rights reserved.
//

import UIKit

class CategoriesModel{
    var maincategory:MainCategoriesModel!
    var subcategory : SubCategoriesModel!
    init(fromData data: [String:Any]) {
        if let maincategoryData = data["maincategory"] as? [String:Any]{
            self.maincategory = MainCategoriesModel(fromData: maincategoryData)
        }
        if let subcategoryData = data["subcategory"] as? [String:Any]{
            self.subcategory = SubCategoriesModel(fromData: subcategoryData)
        }
    }
}
class MainCategoriesModel{
    var datas = [MainCategoriesDataModel]()
    var title : String!
    init(fromData data: [String:Any]) {
        self.title = data["title"] as? String
        if let maincategoryData = data["datas"] as? [[String:Any]]{
            for data in maincategoryData{
                self.datas.append(MainCategoriesDataModel(fromData: data))
            }
        }
    }
}
class MainCategoriesDataModel{
    var api_id : Int!
    var apiname : String!
    var catsubcat : String!
    var image : String!
    var link : String!
    var name : String!
    var type : Int!
    init(fromData data: [String:Any]) {
        self.api_id = data["api_id"] as? Int
        self.apiname = data["apiname"] as? String
        self.catsubcat = data["catsubcat"] as? String
        self.image = data["image"] as? String
        self.link = data["link"] as? String
        self.name = data["name"] as? String
        self.type = data["type"] as? Int
    }
}
class SubCategoriesModel{
    var maincategory = [SubCategories_MainCategoriesModel]()
    var title : String!
    init(fromData data: [String:Any]) {
        self.title = data["title"] as? String
        if let maincategoryData = data["maincategory"] as? [[String:Any]]{
            for data in maincategoryData{
                self.maincategory.append(SubCategories_MainCategoriesModel(fromData: data))
            }
        }
    }
}
class SubCategories_MainCategoriesModel{
    var api_id : String!
    var apiname : String!
    var catsubcat : String!
    var subcategorys = [SubCategories_MainCategories_SubCategoriesModel]()
    var link : String!
    var name : String!
    var type : Int!
    init(fromData data: [String:Any]) {
        self.api_id = data["api_id"] as? String
        self.apiname = data["apiname"] as? String
        self.catsubcat = data["catsubcat"] as? String
        self.link = data["link"] as? String
        self.name = data["name"] as? String
        self.type = data["type"] as? Int
        if let subcategoryData = data["subcategorys"] as? [[String:Any]]{
            for data in subcategoryData{
                self.subcategorys.append(SubCategories_MainCategories_SubCategoriesModel(fromData: data))
            }
        }
    }
}
class SubCategories_MainCategories_SubCategoriesModel{
    var api_id : String!
    var apiname : String!
    var catsubcat : String!
    var image : String!
    var link : String!
    var name : String!
    var type : Int!
    init(fromData data: [String:Any]) {
        self.api_id = data["api_id"] as? String
        self.apiname = data["apiname"] as? String
        self.catsubcat = data["catsubcat"] as? String
        self.image = data["image"] as? String
        self.link = data["link"] as? String
        self.name = data["name"] as? String
        self.type = data["type"] as? Int
    }
}
class NewCategoriesListModel{
    var type : Int!
    var selectedcolor : NewCategoriesList_SelectedColorModel!
    var errorcode : Int!
    var data = [NewCategoriesList_ProductsModel]()
    var message : String!
    init(fromData data: [String:Any]) {
        self.type = data["type"] as? Int
        if let colorData = data["selectedcolor"] as? [String:Any]{
            self.selectedcolor = NewCategoriesList_SelectedColorModel(fromData: colorData)
        }
        self.errorcode = data["errorcode"] as? Int
        if let data = data["data"] as? [[String : Any]]{
            for product in data{
                self.data.append(NewCategoriesList_ProductsModel(fromData: product))
            }
        }
        self.message = data["message"] as? String
    }
}
class NewCategoriesList_SelectedColorModel{
    var background : String!
    var bordercolor : String!
    var color : String!
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.bordercolor = data["border-color"] as? String
        self.color = data["color"] as? String
    }
}
class NewCategoriesList_ProductsModel{
    var api_id : Int!
    var apiid : String!
    var api_name : String!
    var background : String!
    var bordercolor : String!
    var catgoryname : String!
    var catsubcat : String!
    var color : String!
    var active :Int!
    init(fromData data: [String:Any]) {
        self.active = data["active"] as? Int
        self.api_id = data["api_id"] as? Int
        self.apiid = data["api_id"] as? String
        self.api_name = data["api_name"] as? String
        self.background = data["background"] as? String
        self.bordercolor = data["border-color"] as? String
        self.catgoryname = data["catgoryname"] as? String
        self.catsubcat = data["catsubcat"] as? String
        self.color = data["color"] as? String
    }
}
class premiumModel{
    var status     : Int!
    var background : String!
    var border     : String!
    var color      : String!
    var image      : String!
    var text       : String!
    init(fromData data: [String:Any]) {
        self.status = data["status"] as? Int
        self.background = data["background"] as? String
        self.border = data["border"] as? String
        self.color = data["color"] as? String
        self.image = data["image"] as? String
        self.text = data["text"] as? String
    }
}

class MHcategoryProductModel{
    var batchSize : Int!
    var catselectedid : Int!
    var lastPage : Int!
    var product_count : String!
    var premiumtags : premiumModel!
    
    var products = [MHcategoryProduct_ProductModel]()
    init(fromData data: [String:Any]) {
        self.batchSize = data["batchSize"] as? Int
        self.catselectedid = data["catselectedid"] as? Int
        self.lastPage = data["lastPage"] as? Int
        self.product_count = data["product_count"] as? String
        if let products = data["products"] as? [[String:Any]]{
            self.products.removeAll()
            for product in products{
                self.products.append(MHcategoryProduct_ProductModel(fromData: product))
            }
        }
        if let premiumdata = data["premiumtags"] as? [String:Any]{
            self.premiumtags = premiumModel(fromData: premiumdata)
        }
        
    }
}
//MARK:- VDO SECTIONS
class categoryimagemodel{
    var autoplay : Int!
    var imageId : Int!
    var imageUrl : String!
    var gridimage : String!
    var thumpnail : String!
    var type : Int!
    var resolution : Int?
    init(fromData data: [String:Any]) {
        self.autoplay = data["autoplay"] as? Int
        self.imageId = data["imageId"] as? Int
        self.imageUrl = data["imageUrl"] as? String
        self.gridimage = data["gridimage"] as? String
        self.thumpnail = data["thumpnail"] as? String
        self.type = data["type"] as? Int
        self.resolution = data["resolution"] as? Int ?? 0
        
    }
}
class MHcategoryProduct_Product_imagetext_Model{
    var background : String!
    var border : String!
    var color : String!
    var image : String!
    var text : String!
    var status : Int!
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.border = data["border"] as? String
        self.color = data["color"] as? String
        self.image = data["image"] as? String
        self.text = data["text"] as? String
        self.status = data["status"] as? Int
    }
}
class MHcategoryProduct_Product_freesize_Model{
    var background : String!
    var border : String!
    var color : String!
    var text : String!
    var status : Int!
    
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.border = data["border"] as? String
        self.color = data["color"] as? String
        self.text = data["text"] as? String
        self.status = data["status"] as? Int
        print(status)
    }
}
class MHcategoryProduct_ProductModel{
    var description : String!
    var image = [categoryimagemodel]()
    var imagelefttext : MHcategoryProduct_Product_imagetext_Model!
    var toptext : MHcategoryProduct_Product_imagetext_Model!
    var freesize : MHcategoryProduct_Product_freesize_Model!
    var price : String!
    var strikeprice : String!
    var productId : String!
    var productName : String!
    var productTitle : String!
    var qty : String!
    var avalablestock : Int!
    var totalitemprice : String!
    var delivery : String!
    var deliveryMode : String!
    var favorite : Int!
    var mashoAssured : String!
    var offerpercent : String!
    var paymentMode : String!
    var rating : String!
    var returnPolicy : String!
    var shipping : String!
    var offers : String!
    var type : String!
    var viewtype : Int!
//---------------------------------------
    var api_id : Int!
    var apiname : String!
    var button : String!
    var catsubcat : String!
    var heading : String!
    var products = [MHcategoryProduct_ProductModel2]()
//---------------------------------------
    var banners = [bannerImagesModel]()
//---------------------------------------
    var deal : MHcategoryProduct_ProductDealsModel!
    
    init(fromData data: [String:Any]) {
        self.description = data["description"] as? String
//        self.imageUrl = data["image"] as? String
        self.strikeprice = data["strikeprice"] as? String
        self.price = data["specialprice"] as? String
        self.productId = data["productid"] as? String
        self.productName = data["productname"] as? String
        self.productTitle = data["productTitle"] as? String
        self.qty = data["qty"] as? String
        self.avalablestock = data["avalablestock"] as? Int
        self.totalitemprice = data["totalitemprice"] as? String
        self.delivery = data["delivery"] as? String
        self.deliveryMode = data["deliveryMode"] as? String
        self.favorite = data["shortlist"] as? Int
        self.mashoAssured = data["mashoAssured"] as? String
        self.offerpercent = data["offerpercent"] as? String
        self.paymentMode = data["paymentMode"] as? String
        self.rating = data["rating"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.offers = data["offers"] as? String
        self.type = data["type"] as? String
        self.viewtype = data["viewtype"] as? Int
//------------------------------------------------------------------------------
        self.api_id = data["api_id"] as? Int
        self.apiname = data["apiname"] as? String
        self.button = data["button"] as? String
        self.catsubcat = data["catsubcat"] as? String
        self.heading = data["heading"] as? String
        if let products = data["products"] as? [[String:Any]]{
            for product in products{
                self.products.append(MHcategoryProduct_ProductModel2(fromData: product))
            }
        }
        if let banners = data["banner"] as? [[String:Any]]{
            for banner in banners{
                self.banners.append(bannerImagesModel(fromData: banner))
            }
        }
        if let deals = data["deal"] as? [String:Any]{
                self.deal = MHcategoryProduct_ProductDealsModel(fromData: deals)
        }
        if let imagelefttext = data["imagelefttext"] as? [String:Any]{
                self.imagelefttext = MHcategoryProduct_Product_imagetext_Model(fromData: imagelefttext)
        }
        if let toptext = data["toptext"] as? [String:Any]{
                self.toptext = MHcategoryProduct_Product_imagetext_Model(fromData: toptext)
        }
        if let freesize = data["freesize"] as? [String:Any]{
                self.freesize = MHcategoryProduct_Product_freesize_Model(fromData: freesize)
        }
        if let images = data["image"] as? [[String:Any]]{
            image.removeAll()
            for img in images{
                self.image.append(categoryimagemodel(fromData: img))
            }
        }
    }
}

class MHcategoryProduct_ProductDealsModel{
    var background : String!
    var color : String!
    var deal : Int!
    var dealtext : String!
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.color = data["color"] as? String
        self.deal = data["deal"] as? Int
        self.dealtext = data["dealtext"] as? String
    }
}
class MHcategoryProduct_ProductModel2{
    var description : String!
    var imageUrl : String!
    var price : String!
    var strikeprice : String!
    var productId : String!
    var productName : String!
    var productTitle : String!
    var qty : String!
    var avalablestock : Int!
    var totalitemprice : String!
    var delivery : String!
    var deliveryMode : String!
    var favorite : Int!
    var mashoAssured : String!
    var offerpercent : Int!
    var paymentMode : String!
    var rating : String!
    var returnPolicy : String!
    var shipping : String!
    var offers : String!
    var type : String!
    var viewtype : Int!
    init(fromData data: [String:Any]) {
        self.description = data["description"] as? String
        self.imageUrl = data["image"] as? String
        self.strikeprice = data["strikeprice"] as? String
        self.price = data["specialprice"] as? String
        self.productId = data["productid"] as? String
        self.productName = data["productname"] as? String
        self.productTitle = data["productTitle"] as? String
        self.qty = data["qty"] as? String
        self.avalablestock = data["avalablestock"] as? Int
        self.totalitemprice = data["totalitemprice"] as? String
        self.delivery = data["delivery"] as? String
        self.deliveryMode = data["deliveryMode"] as? String
        self.favorite = data["shortlist"] as? Int
        self.mashoAssured = data["mashoAssured"] as? String
        self.offerpercent = data["offerPercentage"] as? Int
        self.paymentMode = data["paymentMode"] as? String
        self.rating = data["rating"] as? String
        self.returnPolicy = data["returnPolicy"] as? String
        self.shipping = data["shipping"] as? String
        self.offers = data["offers"] as? String
        self.type = data["type"] as? String
        self.viewtype = data["viewtype"] as? Int

    }
}
