//
//  NewCategoryDetailsVC.swift
//  masho
//
//  Created by Castler on 29/03/21.
//  Copyright © 2021 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
import CCBottomRefreshControl
import DropDown
import AVFoundation
import AVKit

var delegate : ViewAllProductDelegate!
class NewCategoryDetailsVC: UIViewController {
    var iscategorylistselected = false
    var Device_token : String!
    var UserID : String!
    var TableID : String!
    var userName : String!
    var guest_id : Int! = nil
    var categoryId = ""
    var SubCategoryId = ""
    var nav_title = ""
    var selectedIndex = 0
    var filterData : MHFiltermodel!
    var SelectedCategoryIndex = 0
    var CategoryList : NewCategoriesListModel!
    var Productlist : MHcategoryProductModel!
    var fremodlm : MHcategoryProduct_ProductModel!
    var pdtList : MHcategoryProductModel!
    var FilterCategory = [String:Any]()
//    var api_Key : IntOrString!
    var api_Key : Int! //String!
    var limit = 20
    var totalEntries = 0
    var pageNo = 0
    var getFilterpageNo = 0
    var dropDown = DropDown()
    var DropDwnDataSource = [String]()
    let refreshControl = UIRefreshControl()
    var isFilterApplied = false
    var filterCount = 0
    var searchkey = ""
    var searchvalue = ""
    var isFromSearch = false
    var isSelectedView = "List"
    var currentPlayingIndex: IndexPath?
    var playercontroller = AVPlayerViewController()
    var scrollDirectionDetermined: Bool!
    var offsetObservation : NSKeyValueObservation!
    var vdoplayerdelegate :vdodelegate!
    
    @IBOutlet weak var FilterLb: UILabel!
    @IBOutlet weak var sortImg:UIImageView!
    @IBOutlet weak var cartItemsView: BaseView!
    @IBOutlet weak var CartItemLb: UILabel!
    @IBOutlet weak var NavTitle :UILabel!
    @IBOutlet weak var anchorView: UIView!
    @IBOutlet weak var TopheaderView: UIView!
    
    @IBOutlet weak var HeaderViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var SortView: UIView!
    @IBOutlet weak var SortTypeLb:UILabel!
    @IBOutlet weak var SortTV:UITableView!
    @IBOutlet weak var SortTVHeightConstraint:NSLayoutConstraint!
    @IBOutlet weak var ProductCountlb :UILabel!
    @IBOutlet weak var ProductCV:UICollectionView!
    @IBOutlet weak var categoriesCV:UICollectionView!
    @IBOutlet weak var CategoryListIMG: UIImageView!
    @IBOutlet weak var ListViewIMG: UIImageView!
    @IBOutlet weak var EmptyView: UIView!
    lazy var refreshControl1 = UIRefreshControl()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.appEnteredFromBackground),
                                               name: UIApplication.willEnterForegroundNotification, object: nil)
        CategoryListIMG.image = UIImage(named: "menuiconblack")
        self.EmptyView.isHidden = true
        SortView.isHidden = true
        self.api_Key = Int(categoryId)
       
        userName = UserDefaults.standard.value(forKey: "userName") as? String ?? ""
        Device_token = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
//        self.ProductCV.showAnimatedGradientSkeleton()
        GetProducts()
        getFilterContent()
        if isFromSearch{
            getSearchedFilteredCategoryDetails()
        }else{
            self.categoryDetails(api_key: Int(self.api_Key ?? 0), pageno: pageNo)
        }
        refreshControl.triggerVerticalOffset = 100.0
        
        refreshControl.tintColor = UIColor.MHGold
        ProductCV.bottomRefreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(reloadList), for: .valueChanged)
        offsetObservation = ProductCV.observe(\.contentOffset, options: [.new]) { [weak self] (_, value) in
               guard let self = self, self.presentedViewController == nil else {return}
             guard !self.iscategorylistselected else {return}
               NSObject.cancelPreviousPerformRequests(withTarget: self)
               self.perform(#selector(self.upadateOffset), with: nil, afterDelay: 0.2)
           }
          
        self.initializeView()
    }
    
    //
    private func initializeView(){
        refreshControl1.tintColor = .MHGold
        refreshControl1.addTarget(self,action: #selector(reloadList1(sender:)),for: .valueChanged)
        //refreshControl1.addTarget(self, action: #selector(reloadList), for: .valueChanged)
        ProductCV.refreshControl = refreshControl1
//        ProductCV.addSubview(refreshControl1)
//        ProductCV.alwaysBounceVertical = true
        
    }
    @objc func reloadList1(sender: UIRefreshControl) {
        GetProducts()
        //getFilterContent()
        print("Pull refreshhhh filter contents")
        print("isFromSearch",isFromSearch)
        if isFromSearch {
            getSearchedFilteredCategoryDetails()
        }else{
            print("categoryDetails categoryDetails")
            self.categoryDetails(api_key: self.api_Key, pageno: pageNo)
        }
        ProductCV.reloadData()
        
        
    }

    deinit {
        debugPrint("deinit \(#file)")
    }
//    @objc func reloadList1(){
//            print("pull refresh")
//            GetProducts()
//            getFilterContent()
//            if isFromSearch{
//                getSearchedFilteredCategoryDetails()
//            }else{
//                self.categoryDetails(api_key: Int(self.api_Key ?? 0), pageno: pageNo)
//            }
//
//        }
//    //
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let visibleCells = ProductCV.visibleCells.compactMap { $0 as? newdesigncell }
        guard visibleCells.count > 0 else { return }
        let visibleFrame = CGRect(x: 0, y: ProductCV.contentOffset.y, width: ProductCV.bounds.width, height: ProductCV.bounds.height)
        let visibleCell = visibleCells
            .filter { visibleFrame.intersection($0.frame).height >= $0.frame.height / 2 }
            .first
        let visibleRect = CGRect(origin: ProductCV.contentOffset, size: ProductCV.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        guard let visibleIndexPath = ProductCV.indexPathForItem(at: visiblePoint) else {return}
        if let videoCell = ProductCV.cellForItem(at: visibleIndexPath) as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
            ASVideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
        }
    }
    
    @objc func upadateOffset() {
        pausePlayeVideos()
    }
//MARK: - Thumb generator from url
//    func getThumbnailImage(forUrl url: URL) -> UIImage? {
//        let asset: AVAsset = AVAsset(url: url)
//        let imageGenerator = AVAssetImageGenerator(asset: asset)
//
//        do {
//            let ThumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1) , actualTime: nil)
//            return UIImage(cgImage: ThumbnailImage)
//        } catch let error {
//            print(error)
//        }
//
//        return nil
//    }
    @objc func reloadList(){
        if self.Productlist.lastPage > self.pageNo{
            pageNo += 1
            self.categoryDetails1(api_key: api_Key , pageno: 0)
        }else{
            self.refreshControl.endRefreshing()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        userName = UserDefaults.standard.value(forKey: "userName") as? String ?? ""
        Device_token = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        
        if let cartitemsCount = UserDefaults.standard.value(forKey: "CartItems") as? Int{
            if cartitemsCount == nil || cartitemsCount == 0{
                cartItemsView.isHidden = true
            }else{
                cartItemsView.isHidden = false
                CartItemLb.text = "\(cartitemsCount)"
            }
        }
    }
    @IBAction func FilterBTNtapped(_ sender: Any) {
        if isFilterApplied{
            let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHFilterVC") as! MHFilterVC
            vc.categoryId = self.categoryId
            vc.SubCategoryId = self.SubCategoryId
            vc.delegate = self
            vc.nav_title = NavTitle.text!
            vc.FilterAppliedArray = FilterCategory
            self.navigationController!.pushViewController(vc, animated: true)
        }else{
            let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHFilterVC") as! MHFilterVC
            vc.categoryId = self.categoryId
            vc.SubCategoryId = self.SubCategoryId
            vc.delegate = self
            vc.nav_title = NavTitle.text!
            self.navigationController!.pushViewController(vc, animated: true)
        }
    }
    @IBAction func SortBTNtapped(_ sender: Any) {
        SortView.isHidden = false
        SortTV.reloadData()
    }
    @IBAction func vdoBTn(_ sender: UIButton) {
        if self.Productlist.products[sender.tag].image[0].type == 2 && self.Productlist.products[sender.tag].image[0].autoplay == 0{
            
            self.vdoplayerfullview(url: self.Productlist.products[sender.tag].image[0].imageUrl)
        }
        else {
            if self.Productlist != nil && self.Productlist.products[sender.tag].viewtype == 1{
                                let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
                            //        MHProductDetailsVC.categoryArray = categoryArray
                              MHProductDetailsVC.isFrom = .newCategoryDetails
                               MHProductDetailsVC.BasicData8 = self.Productlist.products[sender.tag]
//                                MHProductDetailsVC.productID = self.Productlist.products[sender.tag].productId
                productID = self.Productlist.products[sender.tag].productId
                                frompush = false
                                MHProductDetailsVC.VideoResolutiontype = self.Productlist.products[sender.tag].image[0].resolution
                                self.navigationController?.pushViewController(MHProductDetailsVC, animated: true)
                            }
        }
        
    }
    //MARK: - Deal view button action
    @IBAction func DealViewAll(_ sender: UIButton) {
        let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
        vc.categoryId = "\(self.Productlist.products[sender.tag].api_id!)"
        vc.SubCategoryId = "sc"
        self.navigationController?.pushViewController(vc, animated: true)    }
    
    @IBAction func dashboardBTNtapped(_ sender: Any) {
        KVSpinnerView.dismiss()
        let navigationStack = navigationController?.viewControllers ?? []
        for vc in navigationStack{
            if let dashboardVC = vc as? MHdashBoardVC{
                DispatchQueue.main.async {
                    self.navigationController?.popToViewController(dashboardVC, animated: false)
                }
            }
        }
    }
    @IBAction func performTap(_ sender: UITapGestureRecognizer) {
        SortView.isHidden = true
    }
    @IBAction func CartBTNtapped(_ sender: Any) {
        let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//        WebViewVC.categoryArray = self.categoryArray
        frompush = false
        self.navigationController?.pushViewController(WebViewVC, animated: true)
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchBTNTapped(_ sender: Any) {
        let search = StoryBoard.login.instantiateViewController(withIdentifier: "searchVC") as! searchVC
        self.navigationController?.pushViewController(search, animated: true)
        
    }
    @IBAction func ShowDropDownBTNtapped(_ sender: Any) {
        dropDown.show()
        setDropdown()
    }
    
    func setDropdown() {
        dropDown.anchorView = anchorView
        dropDown.width = self.view.frame.width / 2.5
        dropDown.dataSource = DropDownDataSource
        //        var imageString = ["Icon-App","Icon-App","Icon-App","Icon-App"]
        dropDown.cellNib = UINib(nibName: "MyDropDownCell", bundle: nil)
        dropDown.customCellConfiguration = {
            (index: Index, item: String, cell: DropDownCell) -> Void in
            guard let cell = cell as? MyDropDownCell else { return }
            //            cell.logoImageView.image = UIImage(named: imageString[index])
            cell.logoImageView.kf.setImage(with: URL(string: DropDownData[index].image))
            cell.backgroundColor = #colorLiteral(red: 0.8296601772, green: 0.6882546544, blue: 0.2148788273, alpha: 1)
        }
        dropDown.dismissMode = .onTap
        DropDown.appearance().cornerRadius = 5
        DropDown.appearance().textFont = UIFont(name: "Roboto-Regular", size: 12)!
//        DropDown.appearance().backgroundColor = #colorLiteral(red: 0.8306156993, green: 0.6888336539, blue: 0.2119885683, alpha: 1)
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if DropDownData[index].type == 1{
                if DropDownData[index].apiname == "homepagedetails"{
                    let navigationStack = self.navigationController?.viewControllers ?? []
                    for vc in navigationStack{
                        if let dashboardVC = vc as? MHdashBoardVC{
                            DispatchQueue.main.async {
                                self.navigationController?.popToViewController(dashboardVC, animated: true)
                            }
                        }
                    }
                }else if DropDownData[index].apiname == "getProductsByCatId"{
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                    vc.categoryId = "\(DropDownData[index].api_id!)"
                    vc.SubCategoryId = "sc"
                    self.navigationController?.pushViewController(vc, animated: true)
                }else  if DropDownData[index].apiname == "getCartItems" {
                    let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
//                    CartVC.categoryArray = self.categoryArray
                    frompush = false
                    self.navigationController?.pushViewController(CartVC, animated: true)
                }else  if DropDownData[index].apiname == "categorys" {
                    let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }else{
                    //getProductDetails
                }
            }else if DropDownData[index].type == 2{
                let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                WebViewVC.WebURL = DropDownData[index].link
                self.navigationController!.pushViewController(WebViewVC, animated: true)
            }else{
                if let newURL = URL(string: DropDownData[index].link){
                    UIApplication.shared.open(newURL)
                }
            }
        }
    }
   //MARK:- //newDesign Buttons
    
    @IBAction func CategoryViewBTN(_ sender: UIButton) {
       
        if iscategorylistselected == true {
        
        CategoryListIMG.image = UIImage(named:"menuiconblack")
        iscategorylistselected = false
        ProductCV.reloadData()
        } else  {
        CategoryListIMG.image = UIImage(named:"listiconblack")
        iscategorylistselected = true
        ProductCV.reloadData()
        }
    
    }
  
}
    extension NewCategoryDetailsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView != categoriesCV{
            return 2
        }else{
            return 1
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoriesCV{
            if CategoryList != nil{
                return CategoryList.data.count
            }else{
                return 0
            }
        }else{
            if section == 0{
                return 1
            }else{
                if self.Productlist != nil {
                    return self.Productlist.products.count
                }else{
                    return 1
                }
            }
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoriesCV{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewCategoriesCVC", for: indexPath) as! NewCategoriesCVC
            if CategoryList != nil{
                cell.TitleLb.text = CategoryList.data[indexPath.row].catgoryname
//                if SelectedCategoryIndex == indexPath.row{
                
                    cell.BGView.backgroundColor =
                        UIColor(hex: "\(CategoryList.data[indexPath.row].background!)ff")
                    cell.BGView.layer.borderColor = UIColor(hex: "\(CategoryList.data[indexPath.row].bordercolor!)ff")?.cgColor//#colorLiteral(red: 0.8306156993, green: 0.6888336539, blue: 0.2119885683, alpha: 1) //SortViewUIColor(hex: "\(CategoryList.selectedcolor.bordercolor!)ff")?.cgColor
                    cell.TitleLb.textColor = UIColor(hex: "\(CategoryList.data[indexPath.row].color!)ff")
//                }else{
////                    cell.BGView.backgroundColor = UIColor(hex: "\(CategoryList.data[indexPath.row].background!)ff")
////                    cell.BGView.layer.borderColor = UIColor(hex: "\(CategoryList.data[indexPath.row].bordercolor!)ff")?.cgColor
//                    cell.BGView.backgroundColor = UIColor.white
//                    cell.BGView.layer.borderColor = UIColor.lightGray.cgColor
//                    cell.TitleLb.textColor = UIColor.white
//                }
            }
            return cell
        }

        
        else{
            if indexPath.section == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PremiumCVC", for: indexPath) as! PremiumCVC
                if self.Productlist != nil {
                    if self.Productlist.premiumtags.status == 1{
                        cell.PremiumBGvew.backgroundColor = UIColor(hex: "\(self.Productlist.premiumtags.background!)ff")
                        cell.PremiumBGvew.layer.borderWidth = 1
                        cell.PremiumBGvew.layer.borderColor = UIColor(hex: "\(self.Productlist.premiumtags.border!)ff")?.cgColor
                        cell.PremiumTxt.text = self.Productlist.premiumtags.text ?? ""
                        cell.PremiumTxt.textColor = UIColor(hex: "\(self.Productlist.premiumtags.color!)ff")
                        cell.PremiumImg.kf.setImage(with: URL(string: self.Productlist.premiumtags.image))
                        cell.PremiumBGvew.isHidden = false
                    }else{
                        cell.PremiumBGvew.isHidden = true
                    }
                }
                return cell
            }else{
                //            let cell = UITableViewCell()
                if self.Productlist != nil && self.Productlist.products[indexPath.row].viewtype == 1{
                    if iscategorylistselected {
                        // MARK: - Grid view Data source
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewCategoriesProductCVC", for: indexPath) as! NewCategoriesProductCVC
                        cell.ImportedView.isHidden = true
                        if  self.Productlist.products[indexPath.row].freesize.status == 1
                          {
//                            cell.FreesizeView
                            cell.FreesizeView.isHidden = false
                            cell.FreesizeView.backgroundColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].freesize.background!)ff")
                            cell.Freesize_Lb.textColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].freesize.color!)ff")
                            cell.Freesize_Lb.text = self.Productlist.products[indexPath.row].freesize.text
                            cell.FreesizeView.layer.borderWidth = 1
                            cell.FreesizeView.layer.borderColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].freesize.border!)ff")?.cgColor
                          }
                          else {
                              cell.FreesizeView.isHidden = true
                              
                          }
                        cell.PercentageLb.text = self.Productlist.products[indexPath.row].offerpercent
                        cell.PercentageView.isHidden = self.Productlist.products[indexPath.row].offerpercent == nil
                        cell.ImportedView.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 2))
                        cell.ImportedViewLeading.constant = -(cell.ImportedView.frame.height / 2) + 5
                        cell.ImportedView.isHidden = true
                        //MARK: - Grid view Resolution
                        if self.Productlist.products[0].image[0].resolution == 2 {
                            let newConstraint2 = cell.GridAspectConstraint.constraintWithMultiplier(1080/1920)
                            cell.Productimage.removeConstraint(cell.GridAspectConstraint)
                            cell.Productimage.addConstraint(newConstraint2)
                            cell.Productimage.layoutIfNeeded()
                            cell.GridAspectConstraint = newConstraint2
                        } else {
                            let newConstraint2 = cell.GridAspectConstraint.constraintWithMultiplier(1080/1440)
                            cell.Productimage.removeConstraint(cell.GridAspectConstraint)
                            cell.Productimage.addConstraint(newConstraint2)
                            cell.Productimage.layoutIfNeeded()
                            cell.GridAspectConstraint = newConstraint2
                        }
                        //
                        cell.delegate = self
                        cell.index = indexPath.row
                        if self.Productlist.products[indexPath.row].deal.deal == 1{
                            cell.DealLb.text = self.Productlist.products[indexPath.row].deal.dealtext
                            cell.DealLb.textColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].deal.color!)ff")
                            cell.DealView.backgroundColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].deal.background!)ff")
                            cell.DealView.isHidden = false
                            cell.PercentageView.isHidden = true
                        }else{
                            cell.DealView.isHidden = true
                            cell.PercentageView.isHidden = false
                            //cell.PercentageLb.text = self.Productlist.products[indexPath.row].offerpercent
                            cell.PercentageLb.text = "\(self.Productlist.products[indexPath.row].offerpercent!)"
                        }
                        cell.DealView.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 4))
                            // MARK: - Grid View Image
                        if self.Productlist.products[indexPath.row].image.count == 0{}
                            else{
                            cell.Productimage.kf.setImage(with: URL(string: self.Productlist.products[indexPath.row].image[0].gridimage))
                        }

                        cell.Title.text = self.Productlist.products[indexPath.row].productName
                        cell.OfferPrice.text = self.Productlist.products[indexPath.row].price
                        cell.OfferPrice.textColor = UIColor.black
                        cell.StrikePrice.textColor = UIColor.red
                        cell.StrikePrice.attributedText = self.Productlist.products[indexPath.row].strikeprice.updateStrikeThroughFont(UIColor.red)
                        if self.Productlist.products[indexPath.row].favorite == 1{
                            cell.FavImg.image = UIImage(named: "favSelected")
                        }else{
                            cell.FavImg.image = UIImage(named: "favUnselected")
                        }
                        
                        cell.ImportedView.isHidden = true
                        DispatchQueue.main.async{
                            if self.Productlist.products[indexPath.row].imagelefttext.status == 1{
                                cell.ImportedView.backgroundColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].imagelefttext.background!)ff")
                                cell.ImportedView.layer.borderWidth = 1
                                cell.ImportedView.layer.borderColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].imagelefttext.border!)ff")?.cgColor
                                cell.Imported_Lb.text = self.Productlist.products[indexPath.row].imagelefttext.text
                                cell.Imported_Lb.textColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].imagelefttext.color!)ff")
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    cell.ImportedView.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 2))
                                    cell.ImportedViewLeading.constant = -(cell.ImportedView.frame.height / 2) + 4
                                    cell.ImportedView.isHidden = false
                                    
                                }
                                
                            }else{
                                cell.ImportedView.isHidden = true
                                
                            }
                        }
                        
                        return cell
                    } else {
                        
//                        if indexPath.row == collectionView.numberOfItems(inSection: 0)-1 && isShimmerNeeded {
//                                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newdesigncell", for: indexPath) as! newdesigncell
//                                    cell.showAnimatedGradientSkeleton()
//                                    return cell
//                                }
                        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newdesigncell", for: indexPath) as! newdesigncell
                    // MARK: - List View Cell
                        cell.OfferLb.text = self.Productlist.products[indexPath.row].offerpercent
                        cell.OfferView.isHidden = self.Productlist.products[indexPath.row].offerpercent == nil
                        cell.VdoBTN.tag = indexPath.item
                        cell.delegate = self
                        cell.indx = selectedIndex
                        cell.index = indexPath.row
                        cell.vdoplayerdelegate = self
                        cell.VideoView.isHidden = true
                        cell.ProductImage.isHidden = false
                        cell.productListing = self.Productlist.products[indexPath.row]
                        cell.navCntrolr = self.navigationController
                        //MARK:- AUTO PLAY = 0
                        if self.Productlist.products[indexPath.row].image[0].type == 2 {
                            
                            if self.Productlist.products[indexPath.row].image[0].autoplay == 0 {
                                cell.videoURL = nil
                               
                                cell.ProductImage.kf.setImage(with: URL(string: self.Productlist.products[indexPath.row].image[0].thumpnail))
                               
                            }else {
                                
                              
                                cell.videoURL = self.Productlist.products[indexPath.row].image[0].imageUrl!
                                cell.ProductImage.kf.setImage(with: URL(string: self.Productlist.products[indexPath.row].image[0].thumpnail))
                            
                                
                            }
                        }else{
                            cell.videoURL = nil
                            cell.ProductImage.kf.setImage(with: URL(string: self.Productlist.products[indexPath.row].image[0].imageUrl))

                        }
                        //MARK: - free size view
                      if  self.Productlist.products[indexPath.row].freesize.status == 1
                        {
                          cell.FreesizeView.isHidden = false
                          cell.FreesizeView.backgroundColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].freesize.background!)ff")
                          cell.FreeSizeLb.textColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].freesize.color!)ff")
                          cell.FreeSizeLb.text = self.Productlist.products[indexPath.row].freesize.text
                          cell.FreesizeView.layer.borderWidth = 1
                          cell.FreesizeView.layer.borderColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].freesize.border!)ff")?.cgColor
                        }
                        else {
                            cell.FreesizeView.isHidden = true
                            
                        }
                        //MARK:- VDOS VIEW IN 2 SIZE (1080/1920 , 1080/1140)
                      //  if self.Productlist.products[indexPath.row].image[0].type == 2 {
                        
//                                if self.Productlist.products[indexPath.row].image[0].autoplay == 1{
                                if self.Productlist.products[indexPath.row].image[0].resolution == 2 {
                                        let newConstraint2 = cell.VideoviewAspectConstraint.constraintWithMultiplier(1080/1920)
                                        cell.Vdoview.removeConstraint(cell.VideoviewAspectConstraint)
                                        cell.Vdoview.addConstraint(newConstraint2)
                                        cell.Vdoview.layoutIfNeeded()
                                        cell.VideoviewAspectConstraint = newConstraint2
                                    } else {
                                        let newConstraint2 = cell.VideoviewAspectConstraint.constraintWithMultiplier(1080/1440)
                                        cell.Vdoview.removeConstraint(cell.VideoviewAspectConstraint)
                                        cell.Vdoview.addConstraint(newConstraint2)
                                        cell.Vdoview.layoutIfNeeded()
                                        cell.VideoviewAspectConstraint = newConstraint2
                                    }
//                                } else {
//                                    let newConstraint2 = cell.VideoviewAspectConstraint.constraintWithMultiplier(8/11)
//                                    cell.Vdoview.removeConstraint(cell.VideoviewAspectConstraint)
//                                    cell.Vdoview.addConstraint(newConstraint2)
//                                    cell.Vdoview.layoutIfNeeded()
//                                    cell.VideoviewAspectConstraint = newConstraint2
//                                }
                            
                        
//                        }else{
//                            //MARK: - Image size (type=1)
//
//                            if self.Productlist.products[indexPath.row].image[0].resolution == 2 {
//                                let newConstraint2 = cell.VideoviewAspectConstraint.constraintWithMultiplier(1080/1920)
//                                cell.Vdoview.removeConstraint(cell.VideoviewAspectConstraint)
//                                cell.Vdoview.addConstraint(newConstraint2)
//                                cell.Vdoview.layoutIfNeeded()
//                                cell.VideoviewAspectConstraint = newConstraint2
//                            } else {
//                                let newConstraint2 = cell.VideoviewAspectConstraint.constraintWithMultiplier(1080/1440)
//                                cell.Vdoview.removeConstraint(cell.VideoviewAspectConstraint)
//                                cell.Vdoview.addConstraint(newConstraint2)
//                                cell.Vdoview.layoutIfNeeded()
//                                cell.VideoviewAspectConstraint = newConstraint2
//                            }
////
////                            let newConstraint2 = cell.VideoviewAspectConstraint.constraintWithMultiplier(8/11)
////                            cell.Vdoview.removeConstraint(cell.VideoviewAspectConstraint)
////                            cell.Vdoview.addConstraint(newConstraint2)
////                            cell.Vdoview.layoutIfNeeded()
////                            cell.VideoviewAspectConstraint = newConstraint2
//                        }
                            
                            
                        
                        
                        DispatchQueue.main.async {
                            cell.Vdoview.layoutIfNeeded()
                            cell.layoutIfNeeded()
                            print("aspect ration of main view",cell.ProductImage.frame.size)
                        }
                        
                        cell.ProductNameLBL.text = self.Productlist.products[indexPath.row].productName
                        if self.Productlist.products[indexPath.row].mashoAssured == "1"{
                            cell.AssuredView.isHidden = false
                        }else{
                            cell.AssuredView.isHidden = true
                        }
                        cell.imageArray.removeAll()
                        var copy = self.Productlist.products[indexPath.row].image
                        copy.removeFirst()
                        cell.imageArray = copy
                        cell.SubProductImageCV.reloadData()

                        cell.StrikedPriceLBL.text =
                            self.Productlist.products[indexPath.row].strikeprice
                        cell.PriceLBL
                            .text = self.Productlist.products[indexPath.row].price
                        cell.StrikedPriceLBL.attributedText = self.Productlist.products[indexPath.row].strikeprice.updateStrikeThroughFont(UIColor.red)
                        
                        if self.Productlist.products[indexPath.row].favorite == 1{
                            cell.FavouriteIMG.image = UIImage(named: "favSelected")
                        }else{
                            cell.FavouriteIMG.image = UIImage(named: "favUnselected")
                        }
                       
                        DispatchQueue.main.async{
                            if self.Productlist.products[indexPath.row].imagelefttext.status == 1{
                                cell.ImportedView.backgroundColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].imagelefttext.background!)ff")
                                cell.ImportedView.layer.borderWidth = 1
                                //
                                cell.ImportedView.clipsToBounds = true
                                cell.ImportedView.layer.cornerRadius = 10
                                cell.ImportedView.layer.maskedCorners =  [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
                                cell.ImportedViewheight.constant = 22
                                
                                //
                                cell.ImportedView.layer.borderColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].imagelefttext.border!)ff")?.cgColor
                                cell.Imported_Lb.text = self.Productlist.products[indexPath.row].imagelefttext.text.capitalized
                                cell.Imported_Lb.textColor = UIColor(hex: "\(self.Productlist.products[indexPath.row].imagelefttext.color!)ff")
                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                                    cell.ImportedView.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi / 2))
                                    cell.ImportedViewLeading.constant = -(cell.ImportedView.frame.height / 2) + 11
                                    cell.ImportedView.isHidden = false
                                }
                                
                            }else{
                                cell.ImportedView.isHidden = true
                            }
                        }
                        
                        return cell
                    }
                    
                }else if self.Productlist != nil && self.Productlist.products[indexPath.row].viewtype == 2{
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterBannerCVC", for: indexPath) as! FilterBannerCVC
                    cell.banners = self.Productlist.products[indexPath.row].banners
                    cell.navCtler = self.navigationController
                    cell.BannerView.reloadData()
                    return cell
                } else {
                    //MARK: - Deal of the day cell
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterDealsOfTheDayCVC", for: indexPath) as! FilterDealsOfTheDayCVC
                    if self.Productlist != nil{
                        cell.TitleLb.text = self.Productlist.products[indexPath.row].heading
                        cell.products = self.Productlist.products[indexPath.row].products
                        cell.ViewallBTN.tag = indexPath.item
                        cell.viwBorder.isHidden = false
                    } else {
                        cell.viwBorder.isHidden = true
                    }
                    cell.navCtlr = self.navigationController
                    cell.ProductsCV.reloadData()
                    return cell
                }
                
                
                //            return cell
            }
        }
    }
        
    
        func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            scrollDirectionDetermined = false
//            /pausePlayeVideos()
        }
        
        func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            if !decelerate {
               // pausePlayeVideos()
            }
        }
        
        func pausePlayeVideos(){
           
            ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: ProductCV)
        }
        
        @objc func appEnteredFromBackground() {
            ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: ProductCV, appEnteredFromBackground: true)
        }
        
    
        
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == categoriesCV{
            let width = (CategoryList.data[indexPath.row].catgoryname as NSString).size(withAttributes: nil).width
            return CGSize(width: width + 40, height: collectionView.frame.height)
        }else{
            if indexPath.section == 0{
                if self.Productlist != nil{
                    if self.Productlist.premiumtags.status == 1{
                        return CGSize(width: collectionView.frame.width, height: 90)
                       
                    }else{
                        return CGSize(width: collectionView.frame.width, height: 1)

                    }
                }else{
                    return CGSize(width: collectionView.frame.width, height: 1)

                }
            }else{
              
                if self.Productlist != nil && self.Productlist.products[indexPath.row].viewtype == 1{
                    if iscategorylistselected{
                        // MARK: - Grid View resolution and cell heights
                        if self.Productlist.products[0].image[0].resolution == 2{
                            print("R2")
                            print(collectionView.frame.height)
                            let width = collectionView.frame.width / 2
                            let multiplier = width / 8
                            let height = 13 * multiplier
                            return CGSize(width: width, height: height + 85)
                        }
                        else{
                           print("R1")
                            let width = collectionView.frame.width / 2
                            let multiplier = width / 8
                            let height = 10 * multiplier
                            return CGSize(width: width, height: height + 85)
                        }
                        
                    }else{
                        // MARK: - VDO RESOLUTIONS r2(524), r1 (131)
                        
                                    if self.Productlist.products[0].image[0].resolution == 2 {
                                        let width = collectionView.frame.width
//                                        let multiplier = width / 387
                                        let multiplier = width / 390
                                        let height = 524 * multiplier
                                        return CGSize(width: width, height: height + 100)
                                      
                                        
                                    }else{
                                        let width = collectionView.frame.width
                                        let multiplier = width / 129
                                        let height = 131 * multiplier
                                        return CGSize(width: width, height: height + 100)
                                    }
                                        
//                                    }else {
//                                        let width = collectionView.frame.width
//                                        let multiplier = width / 187
//                                        let height = 190 * multiplier
//                                        return CGSize(width: width, height: height + 110)
//                                    }
//                                }else{
//                                    if self.Productlist.products[indexPath.row].image[0].resolution == 2 {
//                                        let width = collectionView.frame.width
//                                        let multiplier = width / 387
//                                        let height = 524 * multiplier
//                                        return CGSize(width: width, height: height + 110)
//                                    }else{
//                                        let width = collectionView.frame.width
//                                        let multiplier = width / 129
//                                        let height = 131 * multiplier
//                                        return CGSize(width: width, height: height + 110)
//                                    }
//                                }

                            
                    

                    }
                    // MARK: Detail page banner view cell size
                }else if self.Productlist != nil && self.Productlist.products[indexPath.row].viewtype == 2{
                    return CGSize(width: collectionView.frame.width, height: 220)
                }else{
                    return CGSize(width: collectionView.frame.width, height: 320)
                }
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoriesCV{
            isFilterApplied = false
            FilterCategory = [:]
            FilterLb.text = "Filter"
            if CategoryList.data[indexPath.row].catgoryname == "All category"{
                let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
                self.navigationController!.pushViewController(vc, animated: true)
            }else{
                SelectedCategoryIndex = indexPath.row
                collectionView.reloadData()
                for product in CategoryList.data{
                    print("* ",product.api_id ?? product.apiid)
                }
                if CategoryList.data[indexPath.row].api_id != nil{
                    NavTitle.text = CategoryList.data[indexPath.row].catgoryname
                    SubCategoryId = CategoryList.data[indexPath.row].catsubcat ?? "sc"
                    self.api_Key = (CategoryList.data[indexPath.row].api_id )
                    categoryId = "\(CategoryList.data[indexPath.row].api_id!)"
                    GetProducts()
                    categoryDetails(api_key: (CategoryList.data[indexPath.row].api_id),pageno: 0)
                }else{
                    NavTitle.text = CategoryList.data[indexPath.row].catgoryname
                    SubCategoryId = CategoryList.data[indexPath.row].catsubcat ?? "sc"
                    categoryId = CategoryList.data[indexPath.row].apiid
                    self.api_Key = Int(CategoryList.data[indexPath.row].apiid)
                    GetProducts()
                    categoryDetails(api_key: Int(CategoryList.data[indexPath.row].apiid)!,pageno: 0)
                }
            }
            
            //self.ProductCV.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
        }else{
            if indexPath.section == 1{
//                if self.imageArray[indexPath.row].autoplay == 0{
//                    vdoplayerdelegate.vdoplayerfullview(url: self.imageArray[indexPath.row].imageUrl)
//                }
                //print(indexPath.row)
//                if self.Productlist.products[indexPath.row].image[0].type == 2{
//
//                    if self.Productlist.products[indexPath.row].image[0].autoplay == 0 {
//                        print("test vdo with autoplay 0  ,\(indexPath.row)")
//                        self.vdoplayerfullview(url: self.Productlist.products[indexPath.row].image[0].imageUrl)
//                    }
//                    else {
//                        print("test vdo with autoplay 1  ,\(indexPath.row)")
//                        if self.Productlist != nil && self.Productlist.products[indexPath.row].viewtype == 1{
//                                            let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
//                                        //        MHProductDetailsVC.categoryArray = categoryArray
//                                          MHProductDetailsVC.isFrom = .newCategoryDetails
//                                           MHProductDetailsVC.BasicData8 = self.Productlist.products[indexPath.row]
//                                            MHProductDetailsVC.productID = self.Productlist.products[indexPath.row].productId
//                                            frompush = false
//                                            MHProductDetailsVC.VideoResolutiontype = self.Productlist.products[indexPath.row].image[0].resolution
//                                            self.navigationController?.pushViewController(MHProductDetailsVC, animated: true)
//                                        }
//                    }
//                }
              //  else {
//                    print("test img ,\(indexPath.row)")
                    if self.Productlist != nil && self.Productlist.products[indexPath.row].viewtype == 1{
                                        let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
                                    //        MHProductDetailsVC.categoryArray = categoryArray
                         MHProductDetailsVC.isFrom = .newCategoryDetails
                         MHProductDetailsVC.BasicData8 = self.Productlist.products[indexPath.row]
//                            MHProductDetailsVC.productID = self.Productlist.products[indexPath.row].productId
                         productID = self.Productlist.products[indexPath.row].productId
                                        frompush = false
                                        MHProductDetailsVC.VideoResolutiontype = self.Productlist.products[indexPath.row].image[0].resolution
                                        self.navigationController?.pushViewController(MHProductDetailsVC, animated: true)
                                    }
              //  }
//                if self.Productlist.products[indexPath.row].image[indexPath.row].autoplay == 0  {
//
//                    self.vdoplayerfullview(url: self.Productlist.products[indexPath.row].image[indexPath.row].imageUrl)
//                }
//                 if self.Productlist != nil && self.Productlist.products[indexPath.row].viewtype == 1{
//                     let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
//                     //        MHProductDetailsVC.categoryArray = categoryArray
//                     MHProductDetailsVC.isFrom = .newCategoryDetails
//                     MHProductDetailsVC.BasicData8 = self.Productlist.products[indexPath.row]
//                     MHProductDetailsVC.productID = self.Productlist.products[indexPath.row].productId
//                     frompush = false
//                     MHProductDetailsVC.VideoResolutiontype = self.Productlist.products[indexPath.row].image[0].resolution
//                     self.navigationController?.pushViewController(MHProductDetailsVC, animated: true)
//
//
//
////               if self.Productlist != nil && self.Productlist.products[indexPath.row].viewtype == 1{
////                    let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
////                    //        MHProductDetailsVC.categoryArray = categoryArray
////                    MHProductDetailsVC.isFrom = .newCategoryDetails
////                    MHProductDetailsVC.BasicData8 = self.Productlist.products[indexPath.row]
////                    MHProductDetailsVC.productID = self.Productlist.products[indexPath.row].productId
////                    frompush = false
////                    MHProductDetailsVC.VideoResolutiontype = self.Productlist.products[indexPath.row].image[0].resolution
////                    self.navigationController?.pushViewController(MHProductDetailsVC, animated: true)
//                }
            }
        }
    }
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//            for cell in ProductCV.visibleCells {
//                let indexPath = ProductCV.indexPath(for: cell)
//                print(indexPath)
//                if indexPath != nil{
//                    selectedIndex = indexPath!.row
//                }
//                ProductCV.reloadData()
//            }
//    }
        // MARK: - CATEGORY DETAIL PAGE VIDEO (AUTOPLAY FUNCTIONS)
        public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
          
//            if let cell = cell as? newdesigncell {
//                if  cell.playerC != nil {
//
//                    cell.playerC.seek(to: .zero)
//                    cell.playerC.pause()
//                }
//            }
        }
        public func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            print("End Displaying",indexPath)
            
            if let videoCell = cell as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
                ASVideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
            }

        }
        func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
            scrollDirectionDetermined = false
            
        }
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
 // MARK: -  swipe down for refresh , header hiding
            guard (scrollView as? UICollectionView) == ProductCV else {
                print("scroll return")
                return }
                if !scrollDirectionDetermined {
                    let translation = scrollView.panGestureRecognizer.translation(in: self.view)
                    if translation.y > 10 {
                       
                        self.HeaderViewTopConstraint.constant = 0
                    }
                  else if translation.y > 250 {
                        print("SWIPEDown")
                        for cell in ProductCV.visibleCells {
                                let indexPathforpullrefresh = ProductCV.indexPath(for: cell)
                                print(indexPathforpullrefresh)
                            if indexPathforpullrefresh?.section == 0 {
                                if indexPathforpullrefresh?.item == 0 {
                                    GetProducts()
                                    getFilterContent()
                                    print("Pull refreshhhh filter contents")
                                    print("isFromSearch",isFromSearch)
                                    if isFromSearch {
                                        getSearchedFilteredCategoryDetails()
                                    }else{
                                        print("categoryDetails categoryDetails")
                                        self.categoryDetails(api_key: self.api_Key, pageno: pageNo)
                                    }
                                    ProductCV.reloadData()
                                }
                            }
                        }
                        
                        
                        UIView.animate(withDuration: 0.3,animations: {
//                            self.TopheaderView.transform = .identity//CGAffineTransform(translationX: 0, y: -self.TopheaderView.frame.height)
                            self.view.layoutIfNeeded()
                    
                        }, completion: nil)
                        
                        scrollDirectionDetermined = true
                    }
                    else if translation.y < 0 {
                        print("Up")
                        self.HeaderViewTopConstraint.constant = -161
                        UIView.animate(withDuration: 0.2, animations: {
//                            self.TopheaderView.transform = CGAffineTransform(translationX: 0, y: -self.TopheaderView.frame.height)
                            self.view.layoutIfNeeded()
                        }, completion: nil)
                        
                        scrollDirectionDetermined = true
                    }
                }
            }

//
//        func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//            play()
//
//        }
//
//        public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//            scrollDirectionDetermined = false
//
//
//        }
//        public func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//           play()
//        }
        
        

        
        public func play() {
        
                let visibleCells = ProductCV.visibleCells.compactMap { $0 as? newdesigncell }
                       guard visibleCells.count > 0 else { return }
                       let visibleFrame = CGRect(x: 0, y: ProductCV.contentOffset.y, width: ProductCV.bounds.width, height: ProductCV.bounds.height)
                       let visibleCell = visibleCells
                           .filter { visibleFrame.intersection($0.frame).height >= $0.frame.height / 2 }
                           .first
                       let visibleRect = CGRect(origin: ProductCV.contentOffset, size: ProductCV.bounds.size)
                       let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
                       guard let visibleIndexPath = ProductCV.indexPathForItem(at: visiblePoint) else {return}
            if self.currentPlayingIndex == visibleIndexPath {
                } else {
                    if self.Productlist != nil && self.Productlist.products[visibleIndexPath.row].viewtype == 1 {
                        if iscategorylistselected == false{
                            if let cell = self.ProductCV.cellForItem(at: visibleIndexPath) as? newdesigncell {
                              
                                if self.Productlist.products[visibleIndexPath.row].image[0].type == 2{
                                    if self.Productlist.products[visibleIndexPath.row].image[0].autoplay == 1{
                                      //  cell.ProductImage.isHidden = true
                                        //cell.VideoView.isHidden = false
                                        
                                        DispatchQueue.main.async {
                                            let urlo = self.Productlist.products[visibleIndexPath.row].image[0].imageUrl!
                                            self.currentPlayingIndex = visibleIndexPath
                                            VideoPlayer.shared.setPlayer(inView: cell.VideoView, video: urlo)
                                            
                                            VideoPlayer.shared.configPlayer(control: .play)
                                            cell.VideoView.layoutIfNeeded()
                                        }
                                        
                                        print("Playing Index",visibleIndexPath)
                                        print("Visible Indexpaths", self.ProductCV.indexPathsForVisibleItems)
                                    }else{
                            
                                        //cell.VideoView.isHidden = true
    //
                                       // cell.ProductImage.isHidden = false
//    //
                                    }
                                }else{
                        
                                    //cell.VideoView.isHidden = true

                                //    cell.ProductImage.isHidden = false

                                }
                            }

                        }
                    }
                        
                }
            
        }
}
extension NewCategoryDetailsVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filterData != nil{
//            return 4
            return filterData.sort.options.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortTVC") as! SortTVC
        if filterData != nil{
            cell.TitleLb.text = filterData.sort.options[indexPath.row].text
            cell.img.kf.setImage(with: URL(string: filterData.sort.options[indexPath.row].image))
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SortView.isHidden = true
        self.SortTypeLb.text = filterData.sort.options[indexPath.row].text
        var selectedFilterContent = [String:Any]()
        selectedFilterContent[self.filterData.sort.name] = filterData.sort.options[indexPath.row].value
        getFilteredCategoryDetails(filter:selectedFilterContent, count: filterCount)
    }
}
extension NewCategoryDetailsVC:applyFilterDelegate,FavoriteProductDelegate,vdodelegate{
    func vdoplayerfullview(url: String) {
        let player = AVPlayer(url: URL(string: url)!)
        let vc = AVPlayerViewController()
        vc.player = player

        present(vc, animated: true) {
            vc.player?.play()
        }

    }
    
    
    
    func GetProducts(){
        //https://www.masho.com/api_v3?action=listcategory&catsubcat=ssc&id=850&userId=123&tableid=32323432435234
        print("categid = ",categoryId)
        let parameters = ["action":"listcategory",
                          "catsubcat":SubCategoryId,
                          "id":categoryId,
                          "userId": self.UserID!,
                          "tableid":self.TableID!,
                          "guestId": guest_id!] as [String : Any]
//        self.EmptyView.isHidden = true
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
                KVSpinnerView.dismiss()
            case .success :
                print("success")
                print(response)
                if let data = response as? [String : Any]{
                    self.CategoryList = NewCategoriesListModel(fromData: data)
                }
                var count = 0
                for product in self.CategoryList.data{
                    if product.active == 1{
                         count += 1
                        self.NavTitle.text = product.catgoryname
                    }
                }
                if count == 1{
                    for product in self.CategoryList.data{
                        if product.active == 1{
                            self.NavTitle.text = product.catgoryname
                        }
                    }
                }else{
                    self.NavTitle.text = self.searchvalue.firstCharacterUpperCase()//.capitalized
                }
                let index = self.CategoryList.data.firstIndex { (pdt) -> Bool in
                    pdt.active == 1
                }
                self.categoriesCV.reloadData()
                self.categoriesCV.layoutIfNeeded()
                self.categoriesCV.scrollToItem(at: IndexPath(item: index ?? 0, section: 0), at: .centeredHorizontally, animated: false)

                KVSpinnerView.dismiss()
                self.refreshControl1.endRefreshing()
                self.pausePlayeVideos()
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
                KVSpinnerView.dismiss()
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                KVSpinnerView.dismiss()
            }
        }
    }
    func categoryDetails(api_key: Int,pageno: Int){
        print(FilterCategory)
        let jsonData = try! JSONSerialization.data(withJSONObject: FilterCategory, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
        let parameters = ["action":"getProductsByCatId",
                          "api_id":"\(api_key)",
                          "pageno":"\(pageno)",
                          "batchSize": "\(limit)",
                          "userid" : UserID!,
                          "catsubcat":SubCategoryId,
                          "guest_id": guest_id!,
                          "filter":decoded] as [String : Any]
//        EmptyView.isHidden = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String:Any]{
                    self.Productlist = MHcategoryProductModel(fromData: data)
                }
                self.ProductCountlb.text = self.Productlist.product_count
                if self.Productlist.products.count != 0{
                    self.ProductCV.isHidden = false
                    self.ProductCV.reloadData()
                    self.scrollDirectionDetermined = false
                    DispatchQueue.main.asyncAfter(deadline: .now()+1.0, execute: {
//                    DispatchQueue.main.async {
                     
                        let playIndexPath = IndexPath(item: 0, section: 1)
                        if  let cell = self.ProductCV.cellForItem(at: playIndexPath) as? newdesigncell
                        {
                           let urlo = self.Productlist.products[playIndexPath.row].image[0].imageUrl
//                           cell.ProductImage.image = self.getThumbnailImage(forUrl: URL(string: urlo)!)
                           if self.Productlist.products[playIndexPath.row].image[0].type == 2 {
                               if self.Productlist.products[playIndexPath.row].image[0].autoplay == 1{
                                self.pausePlayeVideos()

                               }
                               else {
                                   //cell.VideoView.isHidden = true
                                 //  cell.ProductImage.alpha = 1
                                 //  cell.ProductImage.kf.setImage(with: URL(string: self.Productlist.products[playIndexPath.row].image[0].imageUrl))
                               }
                               
                           }
                           else{
                         
                           }
                       }
                        
                    })
                    self.ProductCV.reloadData()
                    
                    
                }else{
                    self.ProductCV.isHidden = true
                }
                
                self.EmptyView.isHidden = true
                KVSpinnerView.dismiss()
                self.refreshControl1.endRefreshing()
//                self?.ProductCV.hideSkeleton()
                
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func categoryDetails1(api_key: Int,pageno: Int){
        
        let parameters = ["action":"getProductsByCatId",
                          "api_id":"\(api_key)",
                          "pageno":"\(pageNo)",
                          "batchSize": "\(limit)",
                          "userid" : UserID!,
                          "catsubcat":SubCategoryId,
                          "guest_id": guest_id!,
                          "filter":FilterCategory] as [String : Any]

        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? [String:Any]{
                    self.pdtList = MHcategoryProductModel(fromData: data)
                }
                self.Productlist.products = self.Productlist.products + self.pdtList.products
                self.ProductCountlb.text = self.Productlist.product_count
                if self.Productlist.products.count != 0{
                    self.ProductCV.isHidden = false
                    self.ProductCV.reloadData()
                }else{
                    self.ProductCV.isHidden = true
                }
                self.ProductCV.reloadData()
                self.refreshControl.endRefreshing()
                self.refreshControl1.endRefreshing()
//                self?.ProductCV.hideSkeleton()
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func getFilteredCategoryDetails(filter:[String:Any],count: Int){
        FilterCategory = filter
        pageNo = 0
        filterCount = count
        if count != 0{
            FilterLb.text = "Filter (\(count))"
        }else{
            FilterLb.text = "Filter"
        }
        let sortby = self.FilterCategory["sortby"] as! Int
        print(sortby)
        for options in self.filterData.sort.options{
            if options.value == sortby{
                self.SortTypeLb.text = options.text
            }
        }
        let jsonData = try! JSONSerialization.data(withJSONObject: filter, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
        print(decoded)
        let parameters = ["action":"getProductsByCatId",
                          "api_id":"\(api_Key!)",
                          "pageno":"\(pageNo)",
                          "batchSize": "\(limit)",
                          "userid" : UserID!,
                          "guest_id": guest_id!,
                          "catsubcat":SubCategoryId,
                          "filter":decoded] as [String : Any]
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                self.isFilterApplied = true
                if let data = response["data"] as? [String:Any]{
                    self.Productlist = MHcategoryProductModel(fromData: data)
                }
                self.ProductCountlb.text = self.Productlist.product_count
                if self.Productlist.products.count != 0{
                    self.ProductCV.isHidden = false
                    self.ProductCV.reloadData()
                }else{
                    self.ProductCV.isHidden = true
                }
                self.ProductCV.reloadData()
                KVSpinnerView.dismiss()
                self.refreshControl1.endRefreshing()
//                self?.ProductCV.hideSkeleton()
                
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    
    func getSearchedFilteredCategoryDetails(){
        
        pageNo = 0
        
            FilterLb.text = "Filter"
        
        print(FilterCategory)
        let jsonData = try! JSONSerialization.data(withJSONObject: FilterCategory, options: [])
        let decoded = String(data: jsonData, encoding: .utf8)!
        print(decoded)
        let parameters = ["action":"getProductsByCatId",
                          "api_id":"\(api_Key!)",
                          "pageno":"\(pageNo)",
                          "batchSize": "\(limit)",
                          "userid" : UserID!,
                          "guest_id": guest_id!,
                          "catsubcat":SubCategoryId,
                          "searchkey":searchkey,
                          "searchvalue":searchvalue,
                          "filter":decoded] as [String : Any]
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                self.isFilterApplied = true
                if let data = response["data"] as? [String:Any]{
                    self.Productlist = MHcategoryProductModel(fromData: data)
                }
                self.ProductCountlb.text = self.Productlist.product_count
                if self.Productlist.products.count != 0{
                    self.ProductCV.isHidden = false
                    self.ProductCV.reloadData()
                }else{
                    self.ProductCV.isHidden = true
                }
                self.ProductCV.reloadData()
                KVSpinnerView.dismiss()
                self.refreshControl1.endRefreshing()

            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func getFilterContent(){
        //https://www.masho.com/api_v3?action=filter&catsubcat=ssc&id=860&userId=123&tableid=32323432435234
        let parameters = ["action":"filter",
                          "catsubcat":self.SubCategoryId,
                          "id":self.categoryId,
                          "tableid": TableID!,
                          "userId" : UserID!,
                          "guest_id": guest_id!] as [String : Any]
        //        EmptyView.isHidden = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let data = response as? [String:Any]{
                    self.filterData = MHFiltermodel(fromData: data)
                }
                for data in self.filterData.sort.options{
                    self.DropDwnDataSource.append(data.text)
                }
                self.sortImg.kf.setImage(with: URL(string: self.filterData.sort.options[0].image))
                //["priceto": "10000", "pricefrom": "10", "sortby": 0]
                var filterr = [String:Any]()
                for data in self.filterData.data{
                    if data.filterproperties[0].property_name == "Price"{
                        filterr[data.filterproperties[0].from] = data.filterpropertieitems[0].pricefrom
                        filterr[data.filterproperties[0].to] = data.filterpropertieitems[0].priceto
                        
                    }
                }
                filterr[self.filterData.sort.name] = self.filterData.sort.options[0].value
                
//                self.getFilteredCategoryDetails(filter: filterr)
                
                self.SortTVHeightConstraint.constant = CGFloat(50 * self.DropDwnDataSource.count)
                self.view.layoutIfNeeded()
                self.SortTypeLb.text = self.DropDwnDataSource[0]
                self.SortTV.reloadData()
                KVSpinnerView.dismiss()
                self.refreshControl1.endRefreshing()
                
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func didSelectFavorite(indx: Int) {
    // MARK: - Favourite Refresh (particular cell reload)
        

        let productID = self.Productlist.products[indx].productId ?? ""
        let shortList = self.Productlist.products[indx].favorite
        let deviceToken = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        let userID = UserDefaults.standard.value(forKey: "userID") as? String ?? "" //userID
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        var type:String = ""
        if shortList == 0{
            type = "1"
        }else{
            type = "2"
        }
        let parameters = ["action":"Shortlist",
                          "productid":"\(productID)",
                          "userid":"\(userID)",
                          "guestId": guest_id,
                          "type":"\(type)",
                          "deviceid": deviceToken,
                          "devicetype": 2,
                          "firebaseRegID": "1234"] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if self.Productlist.products[indx].favorite == 0{
                    self.Productlist.products[indx].favorite = 1
                }else{
                    self.Productlist.products[indx].favorite = 0
                }
                if let tvc = self.ProductCV.cellForItem(at: IndexPath(item: indx, section: 1)) as? newdesigncell {

                    if self.Productlist.products[indx].favorite == 1{
                        tvc.FavouriteIMG.image = UIImage(named: "favSelected")
                    }else{
                        tvc.FavouriteIMG.image = UIImage(named: "favUnselected")
                    }
                    
                }
                if let tvc = self.ProductCV.cellForItem(at: IndexPath(item: indx, section: 1)) as? NewCategoriesProductCVC {

                    if self.Productlist.products[indx].favorite == 1{
                        tvc.FavImg.image = UIImage(named: "favSelected")
                    }else{
                        tvc.FavImg.image = UIImage(named: "favUnselected")
                    }
                    
                }


               
                //self.ProductCV.reloadData()
                self.refreshControl1.endRefreshing()
//                DispatchQueue.main.async {
//                    NotificationCenter.default.post(name: Notification.dashboardContentSetup, object: nil)
//                }
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
            case .failure :
                print("failure")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
//                self.EmptyView.isHidden = true

            case .unknown:
                print("unknown")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
//                self.EmptyView.isHidden = true
            }
        }
        
    }
}

extension Dictionary {
    var queryString: String {
        var output: String = ""
        for (key,value) in self {
            output +=  "\(key)=\(value)&"
        }
        output = String(output.characters.dropLast())
        return output
    }
}
extension String {
    func firstCharacterUpperCase() -> String? {
        guard !isEmpty else { return nil }
        let lowerCasedString = self.lowercased()
        return lowerCasedString.replacingCharacters(in: lowerCasedString.startIndex...lowerCasedString.startIndex, with: String(lowerCasedString[lowerCasedString.startIndex]).uppercased())
    }
}

class newdesigncell : UICollectionViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,ASAutoPlayVideoLayerContainer {
    var navCntrolr : UINavigationController?
    //var playerController: ASVideoPlayerController?
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    var videoURL: String? {
        didSet {
            if let videoURL = videoURL {
                ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL)
            }
            videoLayer.isHidden = videoURL == nil
        }
    }
    
    
    var ImgAry = ["banner1","banner2","banner3","banner1","banner2","banner3"]
    var selectedIndex = 0
    var index : Int!
    var indx : Int!
    var imgURL = ""
    var imageArray = [categoryimagemodel]()
   // var SubimageArray = [categoryimagemodel]()
    var delegate: FavoriteProductDelegate!
    var vdoplayerdelegate :vdodelegate!
    var playerC: AVPlayer!
    var playercontrollerC: AVPlayerViewController!
    var avPlayerLayerC: AVPlayerLayer!
    var productListing : MHcategoryProduct_ProductModel!

    @IBOutlet weak var MainProductImageCV:
        UICollectionView!
    @IBOutlet weak var Vdoview: UIView!
    @IBOutlet weak var VdoBTN: UIButton!
    @IBOutlet weak var seperationview: UIView!
    @IBOutlet weak var ProductImage: UIImageView!
    @IBOutlet weak var VideoView:UIView!
    @IBOutlet weak var OfferView:UIView!
    @IBOutlet weak var OfferLb:UILabel!
    @IBOutlet weak var FreeSizeLb:UILabel!
    @IBOutlet weak var Imported_Lb:UILabel!
    @IBOutlet weak var SubProductImageCV: UICollectionView!
    @IBOutlet weak var ImportedView: BaseView!
    @IBOutlet weak var ImportedViewLeading: NSLayoutConstraint!
    @IBOutlet weak var ImportedViewheight: NSLayoutConstraint!
    @IBOutlet weak var FreesizeView: BaseView!
    @IBOutlet weak var ProductNameLBL: UILabel!
    @IBOutlet weak var VideoviewAspectConstraint: NSLayoutConstraint!
    @IBOutlet weak var ProductImageAspectConstraint: NSLayoutConstraint!
    @IBOutlet weak var StrikedPriceLBL: UILabel!
    @IBOutlet weak var PriceLBL: UILabel!
    @IBOutlet weak var AssuredView: BaseView!
    @IBOutlet weak var FavouriteIMG: UIImageView!
    @IBAction func FavoriteBTNtapped(_ sender: Any) {
        delegate.didSelectFavorite(indx: index)
    }
    //MARK: -THUMBNAIL GENERATOR FROM URL
//    func getThumbnailImage(forUrl url: URL) -> UIImage? {
//        let asset: AVAsset = AVAsset(url: url)
//        let imageGenerator = AVAssetImageGenerator(asset: asset)
//
//        do {
//            let ThumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 1) , actualTime: nil)
//            return UIImage(cgImage: ThumbnailImage)
//        } catch let error {
//            print(error)
//        }
//
//        return nil
//    }
//    override func prepareForReuse() {
//        shotImageView.imageURL = nil
//        super.prepareForReuse()
//    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resize
        ProductImage.layer.addSublayer(videoLayer)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        let horizontalMargin: CGFloat = 20
//        let width: CGFloat = bounds.size.width - horizontalMargin * 2
//        let height: CGFloat = (width * 0.9).rounded(.up)
        videoLayer.frame = CGRect(x: 0, y: 0, width: VideoView.frame.width, height: VideoView.frame.height)
    }
    
    func visibleVideoHeight() -> CGFloat {
//        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(shotImageView.frame, from: shotImageView)
//        guard let videoFrame = videoFrameInParentSuperView,
//            let superViewFrame = superview?.frame else {
//             return 0
//        }
//        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return VideoView.frame.size.height
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
      
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubProductImageCVC", for: indexPath) as! SubProductImageCVC
        //cell.ProductImage.image = UIImage(named: ImgAry[indexPath.row].)
//         c
        //-------------------------------------------------//
            if imageArray[indexPath.row].type == 2 {
                if imageArray[indexPath.row].autoplay == 0 {
                    cell.ProductImage.kf.setImage(with: URL(string: self.imageArray[indexPath.row].thumpnail))
                }else{
                    cell.ProductImage.kf.setImage(with: URL(string: self.imageArray[indexPath.row].imageUrl))
                }
                
                //MARK: -thumbnail from video
                //cell.ProductImage.image = getThumbnailImage(forUrl: URL(string: self.imageArray[0].imageUrl)!)
                
            }
            else
            {
                DispatchQueue.main.async {
                    cell.ProductImage.kf.setImage(with: URL(string: self.imageArray[indexPath.row].imageUrl))
                        }
                
            }
//
//        if imageArray[indexPath.row].resolution == 2{
//            let newConstraint2 = cell.SubAspectConstraint.constraintWithMultiplier(1080/1920)
//            cell.ProductImage.removeConstraint(cell.SubAspectConstraint)
//            cell.ProductImage.addConstraint(newConstraint2)
//            cell.ProductImage.layoutIfNeeded()
//            cell.SubAspectConstraint = newConstraint2
//        } else {
//            let newConstraint2 = cell.SubAspectConstraint.constraintWithMultiplier(1080/1440)
//            cell.ProductImage.removeConstraint(cell.SubAspectConstraint)
//            cell.ProductImage.addConstraint(newConstraint2)
//            cell.ProductImage.layoutIfNeeded()
//            cell.SubAspectConstraint = newConstraint2
//        }
////
//            if selectedIndex == indexPath.row{
//                cell.ProductImage.alpha = 0.5
//                cell.borderView.layer.borderColor = UIColor.red.cgColor
//            }else{
//                cell.ProductImage.alpha = 1
//                cell.borderView.layer.borderColor = UIColor.clear.cgColor
//            }
            return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // MARK: - Sub image 3 cell / 4 cell with resolution
        if imageArray[0].resolution == 2 {
            // 1080x192
            let ratio:CGFloat = 1080/1920
            let width = (UIScreen.main.bounds.width - 14) * 0.25 //collectionView.frame.width
            let height = width/ratio
            print("subproduct 1080x1440",CGSize(width: width, height: height))
            return CGSize(width: width, height: height)
        } else {
            // 1080x1440
            let ratio:CGFloat = 1080/1440
            let width = (UIScreen.main.bounds.width - 14) * 0.25 //collectionView.frame.width
            let height = width/ratio
            print("subproduct 1080x1440",CGSize(width: width, height: height))
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.imageArray[indexPath.row].type == 2{
            if self.imageArray[indexPath.row].autoplay == 0{
                vdoplayerdelegate.vdoplayerfullview(url: self.imageArray[indexPath.row].imageUrl)
            }
        }

        else {
            let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
            //        MHProductDetailsVC.categoryArray = categoryArray
            MHProductDetailsVC.isFrom = .newCategoryDetails
            MHProductDetailsVC.BasicData8 = self.productListing
//            MHProductDetailsVC.productID = self.productListing.productId
            productID = self.productListing.productId
    //        frompush = false
            navCntrolr?.pushViewController(MHProductDetailsVC, animated: true)
        }
       
    }
}
protocol vdodelegate {
    func vdoplayerfullview( url:String)
    
    
}
extension UIView {
    func fadeIn(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in }) {
        self.alpha = 0.0

        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.isHidden = false
            self.alpha = 1.0
        }, completion: completion)
    }

    func fadeOut(duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in }) {
        self.alpha = 1.0

        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.isHidden = true
            self.alpha = 0.0
        }, completion: completion)
    }
}
//extension NewCategoryDetailsVC : SkeletonCollectionViewDataSource ,SkeletonCollectionViewDelegate {
//
//    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
//        return "newdesigncell"
//    }
//
//    func collectionSkeletonView(_ skeletonView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        20
//    }
//
//}
