//
//  NewSideMenuVC.swift
//  masho
//
//  Created by Castler on 26/03/21.
//  Copyright © 2021 Appzoc. All rights reserved.
//

import UIKit
import KVSpinnerView
import Firebase
class NewSideMenuVC: UIViewController {
    
    var categoryArray = [categoryListmodel]()
    var SideMenuArray = [SideMenuModel]()
    var SettingsArray = [SideMenuSettingsModel]()
    var ContactUsArray = [SideMenuContactUsModel]()
    var ColorArray = [UIColor]()
    var SideMenucountrydetails : SideMenucountrydetailsModel!
    var WelcomeDic : welcomeModel?
    var Device_token : String!
    var AppVersion : String!
    var UserID : String!
    var TableID : String!
    var userName : String!
    var guest_id : Int! = nil
    var SolidColorArray = [UIColor.red, UIColor.green, UIColor.blue, UIColor.orange, UIColor.brown, UIColor.cyan, UIColor.green, UIColor.magenta, UIColor.purple, UIColor.darkGray, UIColor.black, UIColor.red, UIColor.green,UIColor.blue,UIColor.orange,UIColor.brown,UIColor.cyan,UIColor.green,UIColor.magenta,UIColor.purple,UIColor.darkGray,UIColor.black]
    @IBOutlet weak var SideMenuTV: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoryArray = categoryListArray1
//        self.welcomeMessage.text = "Welcome to Masho.com"
        
    }
    override func viewWillAppear(_ animated: Bool) {
        logEvent()
        Leftmenu()
        userName = UserDefaults.standard.value(forKey: "userName") as? String ?? ""
        Device_token = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        UserID = UserDefaults.standard.value(forKey: "userID") as? String ?? ""
        TableID = UserDefaults.standard.value(forKey: "tableID") as? String ?? ""
        guest_id = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        AppVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        print(AppVersion)
    }
    func logEvent(){
        Analytics.logEvent("Sidemenu_Opened", parameters: [:])
    }
    func checkLogin() -> Bool{
        let logInStatus = UserDefaults.standard.object(forKey: "isLoggedIn") as? Bool ?? false
        return logInStatus
    }
    func redirectToLogin(){
        DispatchQueue.main.async {
            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "MHLoginVC")
            self.navigationController?.push(viewController: vc)
        }
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func UploadImageBTNtapped(_ sender: Any) {
        if checkLogin(){
            let actionSheet = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
            let galleryAction = UIAlertAction(title: "Gallery", style: .default) { _ in
                let imagePicker = UIImagePickerController()
                imagePicker.sourceType = .photoLibrary
                imagePicker.delegate = self
                self.present(imagePicker, animated: false, completion: nil)
            }
            let cameraAction = UIAlertAction(title: "Camera", style: .default) { _ in
                let imagePicker = UIImagePickerController()
                imagePicker.sourceType = .camera
                imagePicker.delegate = self
                self.present(imagePicker, animated: false, completion: nil)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            actionSheet.addAction(galleryAction)
            actionSheet.addAction(cameraAction)
            actionSheet.addAction(cancelAction)
            DispatchQueue.main.async {
                self.present(actionSheet, animated: true, completion: nil)
            }
        }else{
            redirectToLogin()
        }
    }
}
extension NewSideMenuVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 3{
            return SettingsArray.count
        }else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuProfileTVC") as! SideMenuProfileTVC
            cell.ProfileImage.layer.cornerRadius = cell.ProfileImage.frame.height / 2
            cell.transparentView.layer.cornerRadius = cell.transparentView.frame.height / 2
            cell.CameraiconView.layer.cornerRadius = cell.CameraiconView.frame.height / 2
            if WelcomeDic != nil{
                cell.ProfileImage.kf.setImage(with: URL(string: (self.WelcomeDic?.image)!))
                cell.welcomeMessage.text = self.WelcomeDic?.welcome_message
            }
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTVC") as! SideMenuTVC
            cell.ColorArray = self.ColorArray
            cell.SideMenuArray = self.SideMenuArray
            cell.SideMenuCV.reloadData()
            return cell
        }else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCountryCurrencyTVC") as! SideMenuCountryCurrencyTVC
            cell.CountryLb.text = self.SideMenucountrydetails.countryname
            for data in self.SideMenucountrydetails.Currencyselect.datas{
                if data.value == self.SideMenucountrydetails.Currencyid {
                    var firstPart = ""
                    if let range = data.text.range(of: "-") {
                        firstPart = String(data.text[data.text.startIndex..<range.lowerBound])
                        print(firstPart) // print Hello
                    }
                    cell.currencyLb.text = firstPart
                    break
                }
            }
            cell.languageLb.text = self.SideMenucountrydetails.Language
            cell.Flag.kf.setImage(with: URL(string: self.SideMenucountrydetails.image!))
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuOptionsTVC") as! SideMenuOptionsTVC
            cell.SideMenuOptionIcon.kf.setImage(with: URL(string: self.SettingsArray[indexPath.row].image!))
            cell.SideMenuOptionsLb.text = self.SettingsArray[indexPath.row].name
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section != 1{
            return UITableView.automaticDimension
        }else{
            let width = (tableView.frame.width - 20) / 3
            var height:CGFloat = 0
            if self.SideMenuArray.count % 3 == 0{
                height = (width * (95/105)) * (CGFloat(self.SideMenuArray.count) / 3)
            }else{
                height = (width * (95/105)) * ((CGFloat(self.SideMenuArray.count) / 3) + 1)
            }
            return height
//            return 250
        }
    }
}

extension NewSideMenuVC: updateCountryDetailsDelegate{
    func Leftmenu(){
        let parameters = ["action":"leftmenu",
                          "userid": self.UserID,
                          "guestId": guest_id] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response)
                self.SideMenuArray.removeAll()
                if let leftMenuData = response["data"] as? [[String : Any]]{
                    for data in leftMenuData{
                        self.SideMenuArray.append(SideMenuModel(fromData: data))
                    }
                }
                if let WelcomeData = response["logindetails"] as? [String : Any]{
                    self.WelcomeDic = welcomeModel(fromData: WelcomeData)
                }
                if let countrydetails = response["countrydetails"] as? [String : Any]{
                    self.SideMenucountrydetails = SideMenucountrydetailsModel(fromData: countrydetails)
                }
                if let SettingsData = response["settings"] as? [[String : Any]]{
                    self.SettingsArray = [SideMenuSettingsModel]()
                    for item in SettingsData{
                        self.SettingsArray.append(SideMenuSettingsModel(fromData: item))
                    }
                }
                if let contactusData = response["contactus"] as? [[String : Any]]{
                    self.ContactUsArray = [SideMenuContactUsModel]()
                    for item in contactusData{
                        self.ContactUsArray.append(SideMenuContactUsModel(fromData: item))
                    }
                }
                self.ColorArray.removeAll()
                self.SolidColorArray.shuffle()
                for i in 0 ..< self.SideMenuArray.count{
                    self.ColorArray.append(self.SolidColorArray[i])
                }
                
                self.SideMenuTV.reloadData()
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func updateCountryDetails(CurrencyCode: String,languageCode: String,CountryCode : String,countryName: String ){
        UserDefaults.standard.set(countryName, forKey: "CountryName")
        let parameters = ["action" : "updatecountrysettings",
                          "userId" : self.UserID!,
                          "guestId": guest_id!,
                          "currency" : CurrencyCode,
                          "language" : languageCode,
                          "country" : CountryCode,
                          "countryname" : countryName] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                
                isLanguageUpdated = true
                
                self.Leftmenu()
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func logout(){
        //https://www.masho.com/api?action=logout&Userid=731&tableid=TdNwLUfpD7xORIn
        let parameters = ["action" : "logout",
                          "Userid" : self.UserID!,
                          "tableid": self.TableID!] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                UserDefaults.standard.set("", forKey: "userName")
                UserDefaults.standard.set("", forKey: "userID")
                UserDefaults.standard.set("", forKey: "tableID")
                UserDefaults.standard.set(0, forKey: "guestId")
                UserDefaults.standard.set(false, forKey: "isLoggedIn")
                
                let LoginVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHLoginVC") as! MHLoginVC
                self.navigationController?.pushViewController(LoginVC, animated: false)
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
extension NewSideMenuVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.originalImage] as! UIImage
        if let jpegData = tempImage.jpegData(compressionQuality: 0.8){
            uploadUserImage(image: jpegData)
        }
        DispatchQueue.main.async {
            let cell = self.SideMenuTV.cellForRow(at: IndexPath(row: 0, section: 0)) as! SideMenuProfileTVC
            cell.transparentView.isHidden = true
            cell.ProfileImage.image = tempImage
        }
        
        picker.dismiss(animated: true) {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: false, completion: nil)
    }
    
    func uploadUserImage(image:Data){
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        let userID = UserDefaults.standard.object(forKey: "userID") as? String ?? ""
        Webservice(url: "", parameters: ["userid":"\(userID)","action":"uploadphotoapi"]).executeMultipartPOST(dataParameters: ["image":image], mimeType: "image/jpeg", shouldReturn: .json) { (json, didComplete, response) in
            print(json)
            DispatchQueue.main.async {
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
            }
            
        }
    }
    
}
