//
//  WebViewVC.swift
//  masho
//
//  Created by Appzoc on 06/03/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import WebKit
import KVSpinnerView
class WebViewVC: UIViewController,WKNavigationDelegate {
    var WebURL = ""
    var titleText:String = ""
    var isFromCart = false
    var url : URL? = nil
    var paymentwebpages:MHPaymentWebpageOptionsModel!
    @IBOutlet weak var WebView: WKWebView!
    @IBOutlet weak var titleBarLBL: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleBarLBL.text = titleText
        
        WebView.navigationDelegate = self
        
        print(WebURL)
        let tableID = UserDefaults.standard.object(forKey: "tableID") as? String ?? ""
        let userID = UserDefaults.standard.object(forKey: "userID") as? String ?? ""
        //let url = URL(string: "https://www.masho.com/" + WebURL)
        if isFromCart{
            url = URL(string: WebURL)
        }else{
            url = URL(string: WebURL + "?devicetableid=\(tableID)&userId=\(userID)&device=app")
//            https://www.masho.com/checkout?userId=813&devicetableid=Kx6nqcMCAQwoIk4&device=app
        }
        let request = URLRequest(url: url!)
        WebView.load(request)
    }
    
    
    @IBAction func BackBTNtapped(_ sender: Any) {
        KVSpinnerView.dismiss()
        if frompush {
          let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
            self.navigationController?.pushViewController(dashBoardVC, animated: true)
            
        }
            
        else {
            
        self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    
    @IBAction func titleBarBTNTapped(_ sender: UIButton) {
        KVSpinnerView.dismiss()
        let navigationStack = navigationController?.viewControllers ?? []
        for vc in navigationStack{
            if let dashboardVC = vc as? MHdashBoardVC{
                DispatchQueue.main.async {
                    self.navigationController?.popToViewController(dashboardVC, animated: false)
                }
            }
        }
    }
    
//    func webViewDidStartLoad(_ webView: WKWebView){
//        // show indicator
//
//    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        KVSpinnerView.show()
    }
//    func webViewDidFinishLoad(_ webView: WKWebView){
//        // hide indicator
//
//    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        KVSpinnerView.dismiss()
    }
//    func webView(_ webView: WKWebView, didFailLoadWithError error: Error){
//        // hide indicator
//
//    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        KVSpinnerView.dismiss()
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString {
            //urlStr is what you want
            print(urlStr)
            let url = URL(string: urlStr)
            if url?.host != nil && url!.path != nil && url!.scheme != nil{
                let currentPageURL = url!.scheme! + "://" + url!.host! + url!.path
                print(currentPageURL)
                if paymentwebpages != nil{
                    if currentPageURL == paymentwebpages.confirm{
                        let vc = StoryBoard.new.instantiateViewController(withIdentifier: "SuccessfullViewController") as! SuccessfullViewController
                        self.navigationController!.pushViewController(vc, animated: true)
                    }
                    if currentPageURL == paymentwebpages.failure{
                        let vc = StoryBoard.new.instantiateViewController(withIdentifier: "FailureViewController") as! FailureViewController
                        self.navigationController!.pushViewController(vc, animated: true)
                    }
                }
                
            }
        }

        decisionHandler(.allow)
    }
   

}
