//
//  DashboardCells.swift
//  masho
//
//  Created by Appzoc on 27/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import FSPagerView
import KVSpinnerView
extension String {
  var withoutPunctuations: String {
    return self.components(separatedBy: CharacterSet.punctuationCharacters).joined(separator: "")
  }
}
protocol ViewAllProductDelegate{
    func DidSelectViewAll(index: Int)
}
class MHheaderCategoryTVC: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    var navnCtler : UINavigationController!
    @IBOutlet weak var CategoryCV: UICollectionView!
    //    var categoryArray = ["Abayas - Burqas","Hijabs - carves - Wraps","Full Sleeve Kurti's","Bags"]
    var categoryArray = [categoryListmodel]()
    var ColorArray = [UIColor]()
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "MHheaderCategoryCVC", for: indexPath) as! MHheaderCategoryCVC
        cel.CategoryLb.text = categoryArray[indexPath.row].name
        let originalString = categoryArray[indexPath.row].image
        let convertedstring = originalString?.withoutPunctuations
      
        let escapedString = originalString?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        print("original img ",originalString,"converted= ",escapedString)
        cel.CategoryIcon.kf.setImage(with: URL(string:originalString ?? "" ))
        DispatchQueue.main.async { [self] in
            cel.BackgroundColorView.backgroundColor = ColorArray[indexPath.row].withAlphaComponent(0.1)
        }
        return cel
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3.2, height: collectionView.frame.height - 20)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if categoryArray[indexPath.row].name != "All category"{
            let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
            print("Api id from data if int api_id---",categoryArray[indexPath.row].api_id ?? 0)
            print("Api id from data if string apiid-- ",categoryArray[indexPath.row].apiid ?? "0")
            if categoryArray[indexPath.row].api_id != nil{
                vc.api_Key = categoryArray[indexPath.row].api_id
                vc.categoryId =  "\(categoryArray[indexPath.row].api_id ?? 0)"
            } else {
                vc.api_Key = Int(categoryArray[indexPath.row].apiid ?? "")
                vc.categoryId =  categoryArray[indexPath.row].apiid ?? ""
                }
            
//            vc.categoryId = "\(categoryArray[indexPath.row].api_id!)"
            
            
            
            vc.SubCategoryId = "sc"
            self.navnCtler?.pushViewController(vc, animated: true)
        }else{
            let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "MHCategoriesVC") as! MHCategoriesVC
//            self.ColorArray.shuffle()
//            vc.colorArray = self.ColorArray
            print("HI")
//                WebViewVC.WebURL = SideMenuArray[indexPath.row].api
            self.navnCtler?.pushViewController(vc, animated: true)
        }
        
    }
}





class MHheaderCategoryCVC: UICollectionViewCell{
    @IBOutlet weak var BackgroundColorView : UIView!
    @IBOutlet weak var CategoryIcon: UIImageView!
    @IBOutlet weak var CategoryLb: UILabel!
}




protocol scrollToTopDelegate{
    func scrollToTop()
}


class MHProductsTVC: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,FavoriteProductDelegate{
   
    var delegatee : scrollToTopDelegate!
    @IBOutlet weak var ShowAllBTNColorView: BaseView!
    @IBOutlet weak var BackGroundView: UIView!
    @IBOutlet weak var ProductCV: UICollectionView!
    @IBOutlet weak var categoryNameLb: UILabel!
    @IBOutlet weak var productVCheight: NSLayoutConstraint!
    
    var ProductArray = [ProductModel]()
    var delegate : ViewAllProductDelegate!
    var index : Int!
    var navnCtler : UINavigationController!
    var categoryArray = [categoryListmodel]()
    func redirectToLogin() {
        DispatchQueue.main.async {
            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "MHLoginVC")
            self.navnCtler.push(viewController: vc)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if ProductArray.count >= 4 {
            return 4
//            productVCheight.constant = 610
        }else if ProductArray.count <= 4 {
//            productVCheight.constant = 305
            return ProductArray.count
        }
        else {
            return 4
        }
        //productVCheight.constant = 50
//        return ProductArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "MHProductsCVC", for: indexPath) as! MHProductsCVC
        cel.ProductName.text = ProductArray[indexPath.row].productname
        cel.Productimage.kf.setImage(with: URL(string: ProductArray[indexPath.row].image))
        cel.Price.text = "\(ProductArray[indexPath.row].specialprice!)"
        cel.PricePercentage.text = "\(ProductArray[indexPath.row].offerPercentage!) % Off"
        if ProductArray[indexPath.row].shortlist == "1"{
            cel.FavImg.image = UIImage(named: "favSelected")
        }else{
            cel.FavImg.image = UIImage(named: "favUnselected")
        }
        cel.OfferPrice.textColor = UIColor.red
        cel.OfferPrice.attributedText = "\(ProductArray[indexPath.row].strikeprice!)".updateStrikeThroughFont(UIColor.red)
        cel.delegate = self
        cel.index = indexPath.row
        return cel
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2.1, height: collectionView.frame.height / 2.1)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        //delegatee.scrollToTop()
//        let loginStatus = UserDefaults.standard.object(forKey: "isLoggedIn") as? Bool ?? false
//        if loginStatus == false{
//            redirectToLogin()
//        }else{
            let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
//        MHProductDetailsVC.categoryArray = categoryArray
        MHProductDetailsVC.isFrom = .dashboard
        MHProductDetailsVC.BasicData = ProductArray[indexPath.row]
//        MHProductDetailsVC.productID = ProductArray[indexPath.row].productID
        productID = ProductArray[indexPath.row].productID
         frompush = false
            self.navnCtler.pushViewController(MHProductDetailsVC, animated: true)
//            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//            WebViewVC.WebURL = "https://www.masho.com/" + ProductArray[indexPath.row].productlink
//            self.navnCtler.pushViewController(WebViewVC, animated: true)
//        }
    }
    
    func didSelectFavorite(indx: Int) {
        
        //FavAPI
        print("Product Name : \(ProductArray[indx].productname) \n \(ProductArray[indx].image)")
        let productID = ProductArray[indx].productID ?? ""
        let shortList = ProductArray[indx].shortlist
        let deviceToken = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
        let userID = UserDefaults.standard.value(forKey: "userID") as? String ?? "" //userID
        let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
        var type:String = ""
        if shortList == "0"{
            type = "1"
        }else{
            type = "2"
        }
        let parameters = ["action":"Shortlist",
                          "productid":"\(productID)",
                          "userid":"\(userID)",
                          "guestId": guest_id,
                          "type":"\(type)",
                          "deviceid": deviceToken,
                          "devicetype": 2,
                          "firebaseRegID": "1234"] as [String : Any]
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if self.ProductArray[indx].shortlist == "0"{
                    self.ProductArray[indx].shortlist = "1"
                }else{
                    self.ProductArray[indx].shortlist = "0"
                }
                self.ProductCV.reloadData()
//                DispatchQueue.main.async {
//                    NotificationCenter.default.post(name: Notification.dashboardContentSetup, object: nil)
//                }
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
            case .failure :
                print("failure")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
//                self.view.alpha = 1
//                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
        
    }
    
    @IBAction func ViewAllProduCts(_ sender: Any) {
        delegate.DidSelectViewAll(index: index)
    }
    
    
    
}


protocol FavoriteProductDelegate {
    func didSelectFavorite(indx: Int)
//    func redirectToLogin()
}



class MHProductsCVC: UICollectionViewCell{
    var delegate: FavoriteProductDelegate!
    var index : Int!
    @IBOutlet weak var PricePercentage: UILabel!
    @IBOutlet weak var Productimage: UIImageView!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var OfferPrice: UILabel!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var FavView: BaseView!
    @IBOutlet weak var FavImg: UIImageView!
    
    @IBAction func Fav(_ sender: UIButton) {
//        let isLoggedIn = UserDefaults.standard.object(forKey: "isLoggedIn") as? Bool ?? false
//        if isLoggedIn{
            delegate.didSelectFavorite(indx: index)
//        }else{
//            delegate.redirectToLogin()
//        }
    }
}


class MHBanner2TVC: UITableViewCell,FSPagerViewDelegate,FSPagerViewDataSource{
    var banners = [bannerImagesModel]()
    var navCtler : UINavigationController!
    @IBOutlet weak var BannerView: FSPagerView!
    @IBOutlet weak var PageCtl: FSPageControl!
    override func awakeFromNib() {
        super.awakeFromNib()
        BannerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "SliderCell")
        BannerView.itemSize = CGSize(width: BannerView.frame.width, height: BannerView.frame.height)
        BannerView.transformer = FSPagerViewTransformer(type:.zoomOut)
        
        PageCtl.numberOfPages = self.banners.count//dot
        PageCtl.setImage(UIImage(named: "dot"), for: UIControl.State.selected)
        PageCtl.setImage(UIImage(named: "dot1"), for: UIControl.State.normal)
    }
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return banners.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cel = pagerView.dequeueReusableCell(withReuseIdentifier: "SliderCell", at: index)
//        cel.imageView?.image = UIImage(named: banners[index].image!)
        cel.imageView?.kf.setImage(with: URL(string: banners[index].image!))
        return cel
    }
    
//    func pagerView(_ pagerView: FSPagerView, shouldSelectItemAt index: Int) -> Bool {
//        return false
//    }
    func pagerViewDidScroll(_ pagerView: FSPagerView) {
        if pagerView == self.BannerView{
            self.PageCtl.currentPage = pagerView.currentIndex
        }
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        if banners[index].type == 1{
            if banners[index].apiname == "homepagedetails"{
                let navigationStack = navCtler?.viewControllers ?? []
                for vc in navigationStack{
                    if let dashboardVC = vc as? MHdashBoardVC{
                        DispatchQueue.main.async {
                            self.navCtler?.popToViewController(dashboardVC, animated: true)
                        }
                    }
                }
            }else if banners[index].apiname == "getProductsByCatId"{
                let vc = StoryBoard.filter.instantiateViewController(withIdentifier: "NewCategoryDetailsVC") as! NewCategoryDetailsVC
                vc.categoryId = "\(banners[index].api_id!)"
                vc.SubCategoryId = "sc"
                navCtler?.pushViewController(vc, animated: true)
            }else if banners[index].apiname == "getCartItems" {
                let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                //                CartVC.categoryArray = self.categoryListArray
                frompush = false
                navCtler?.pushViewController(CartVC, animated: true)
            }else if banners[index].apiname == "getProductDetails" {
                let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
                //        MHProductDetailsVC.categoryArray = categoryArray
                MHProductDetailsVC.isFrom = .other
//                MHProductDetailsVC.productID = String(banners[index].api_id)
                productID = String(banners[index].api_id)
                 frompush = false
                navCtler!.pushViewController(MHProductDetailsVC, animated: true)
            }else{
                //
            }
        }else if banners[index].type == 2{
            let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
            WebViewVC.WebURL = banners[index].link
            navCtler!.pushViewController(WebViewVC, animated: true)
        }else{
            if let newURL = URL(string: banners[index].link){
                UIApplication.shared.open(newURL)
            }
        }
    }
}
class MHNewProductsTVC: UITableViewCell{
    var delegate: FavoriteProductDelegate!
    var index : Int!
    @IBOutlet weak var ColorView: UIView!
    @IBOutlet weak var ProductImage1: UIImageView!
    @IBOutlet weak var ProductImage2: UIImageView!
    @IBOutlet weak var ProductImage3: UIImageView!
    @IBOutlet weak var ProductImage4: UIImageView!
    @IBOutlet weak var OfferPercentage: UILabel!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var StrikePrice: UILabel!
    @IBOutlet weak var specialPrice: UILabel!
    @IBOutlet weak var FavImage: UIImageView!
    @IBOutlet weak var OfferPercentage2Lb: UILabel!
    @IBAction func FavBTNtapped(_ sender: Any) {
        delegate.didSelectFavorite(indx: index)
    }
    
}
class recentProductsInDashboardCVC: UICollectionViewCell{
    
    var delegate: FavoriteProductDelegate!
    var index : Int!
    
    @IBOutlet weak var ProductImage: UIImageView!
    @IBOutlet weak var ProductName: UILabel!
    @IBOutlet weak var StrikePrice: UILabel!
    @IBOutlet weak var PriceLb: UILabel!
    @IBOutlet weak var PrecentageLb: UILabel!
    @IBOutlet weak var FavImage: UIImageView!
    @IBOutlet weak var viwPercentage: BaseView!
    
    @IBAction func FavoriteBTNtapped(_ sender: Any) {
        delegate.didSelectFavorite(indx: index)
    }
}
class recentProductsInDashboardTVC: UITableViewCell,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,FavoriteProductDelegate{
    func redirectToLogin() {
        //gdnbdcgh
    }
    var categoryArray = [categoryListmodel]()
    var ProductArray = [recentlyviewedProductModel]()
    var navnCtler : UINavigationController!
    let userID = UserDefaults.standard.value(forKey: "userID") as? String ?? "" //userID
    let guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    @IBOutlet weak var ProductCV: UICollectionView!
    @IBOutlet weak var NewArrivalLb: UILabel!
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ProductArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cel = collectionView.dequeueReusableCell(withReuseIdentifier: "recentProductsInDashboardCVC", for: indexPath) as! recentProductsInDashboardCVC
        cel.ProductName.text = ProductArray[indexPath.row].productName
        cel.ProductImage.kf.setImage(with: URL(string: ProductArray[indexPath.row].imageUrl))
        cel.PriceLb.text = "\(ProductArray[indexPath.row].specialPrice!)"
        cel.PrecentageLb.text = "\(ProductArray[indexPath.row].offerPercentage ?? 0)% Off"
        cel.viwPercentage.isHidden = ProductArray[indexPath.row].offerPercentage == 0
        if ProductArray[indexPath.row].favorite == 1{
            cel.FavImage.image = UIImage(named: "favSelected")
        }else{
            cel.FavImage.image = UIImage(named: "favUnselected")
        }
        cel.PriceLb.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
//            UIColor(hex: "\(ProductArray[indexPath.row].pricecolor!)ff")
        cel.StrikePrice.textColor =
            #colorLiteral(red: 0.9978068471, green: 0.1100951508, blue: 0.1035301015, alpha: 1)
//            UIColor(hex: "\(ProductArray[indexPath.row].sp_pricecolor!)ff")
        cel.StrikePrice.attributedText = "\(ProductArray[indexPath.row].specialPrice!)".updateStrikeThroughFont(#colorLiteral(red: 0.9978068471, green: 0.1100951508, blue: 0.1035301015, alpha: 1))
        cel.delegate = self
        cel.index = indexPath.row
        return cel
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2.3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
        let MHProductDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
//        MHProductDetailsVC.categoryArray = categoryArray
        MHProductDetailsVC.isFrom = .dashboardRecent
        MHProductDetailsVC.BasicData1 = ProductArray[indexPath.row]
//        MHProductDetailsVC.productID = ProductArray[indexPath.row].productId
        productID = ProductArray[indexPath.row].productId
         frompush = false
        self.navnCtler.pushViewController(MHProductDetailsVC, animated: true)

    }
    func didSelectFavorite(indx: Int){
        var type:String = ""
        if ProductArray[indx].favorite == 0{
            type = "1"
        }else{
            type = "2"
        }
        let parameters = [  "action":"Shortlist",
                            "userid":"\(userID)",
                            "guestId": guest_id,
                            "productid": "\(ProductArray[indx].productId!)",
                            "type" : "\(type)"] as [String : Any]
        
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                
                KVSpinnerView.dismiss()
                print("success")
                if self.ProductArray[indx].favorite == 0{
                    self.ProductArray[indx].favorite = 1
                }else{
                    self.ProductArray[indx].favorite = 0
                }
                self.ProductCV.reloadData()
            case .failure :
                print("failure")
                
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
