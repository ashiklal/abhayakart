//
//  loginVC.swift
//  masho
//
//  Created by Appzoc on 26/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import Alamofire
import KVSpinnerView
class MHLoginVC: UIViewController,UIScrollViewDelegate {
    let Device_token : String = (UserDefaults.standard.value(forKey: "Device_Token") ?? "assdssdssd") as! String
    @IBOutlet weak var PhoneTF: UITextField!
    @IBOutlet weak var UnderLine: UIView!
    @IBOutlet weak var CountryCodeLb: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    var guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    var mobcode : String!
    var loginData : LoginModel!
    var CountryCode : String = "IN"
    var CountryName : String = "India"
    let locale = Locale.current
    var GuestData : guestModel! = nil
    var countryCodeArray = [CountryCodeModel]()
//    var JsonResponse : Json!
//    
    override func viewDidLoad() {
        super.viewDidLoad()
        PhoneTF.resignFirstResponder()
        PhoneTF.delegate = self
        PhoneTF.textContentType = .oneTimeCode
        UnderLine.backgroundColor = UIColor.lightGray
        PhoneTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked(_:)))
        getCommonData()
        scroll.scrollToBottom(animated: false)
        scroll.bounces = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        PhoneTF.resignFirstResponder()
    }
    @objc func doneButtonClicked(_ sender: Any) {
        var f = self.view.frame
        f.origin.y = 0
        self.view.frame = f
    }
    
    @IBAction func SkipLoginBTNtapped(_ sender: Any) {
        
        
        if guest_id == 0 {
//            self.CountryCode = locale.regionCode!
//            self.CountryName = countryName(from: locale.regionCode!)
            guestlogin()
        }else{
            isFromCartCheckout = false
            addReviewBeforeLogin = false
            ViewReviewBeforeLogin = false
            isLogged = false
            self.navigationController?.popViewController(animated: true)
        }
//        let navigationStack = navigationController?.viewControllers ?? []
//        for vc in navigationStack{
//            if let dashboardVC = vc as? MHdashBoardVC{
//                DispatchQueue.main.async {
//                    self.navigationController?.popToViewController(dashboardVC, animated: true)
//                }
//            }
//        }
//        let DashboardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
//        self.navigationController?.pushViewController(DashboardVC, animated: false)
    }
    
    @IBAction func sendOTP_BTNtapped(_ sender: Any) {
        PhoneTF.resignFirstResponder()
//        let valid_PhNo = BaseValidator.isValid(digits: PhoneTF.text ?? "")
        if PhoneTF.text == ""{
            Banner.main.showBanner(title: "", subtitle: "Please enter your mobile number", style: .danger)
//        }else if (PhoneTF.text?.count ?? 0) < 10{
//            Banner.main.showBanner(title: "", subtitle: "Mobile number must be 10 digits", style: .danger)
        }else{
//            if valid_PhNo{
                Login()
                
//            }else{
//                Banner.main.showBanner(title: "", subtitle: "Enter valid Mobile number", style: .danger)
//            }
        }
    }
    
    @IBAction func CountryCodeBTNtapped(_ sender: Any) {
        if CountryCodeLb.text != ""{
            let codeVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCountryCodeVC") as! MHCountryCodeVC
            codeVC.countryCodeArray = self.countryCodeArray
            codeVC.screenShot = self.view.takeScreenshot()
            codeVC.delegate = self
            self.navigationController?.pushViewController(codeVC, animated: false)
        }
    }
    
    @IBAction func RegisterBTNtapped(_ sender: Any) {
        PhoneTF.resignFirstResponder()
        let RegVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHRegisterVC") as! MHRegisterVC
        RegVC.countryCodeArray = self.countryCodeArray
        self.navigationController?.pushViewController(RegVC, animated: true)
    }
    func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return countryCode
        }
    }
}

extension MHLoginVC: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print("ooooooooooooooooooo\(keyboardHeight)")
        var f = self.view.frame
        if screenHeight / screenWidth < 2{
            f.origin.y = -265
        }else{
            f.origin.y = -345
        }
        self.view.frame = f
        UnderLine.backgroundColor = UIColor.MHGold
        return true
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == PhoneTF{
            var f = self.view.frame
            f.origin.y = 0
            self.view.frame = f
            textField.resignFirstResponder()
            UnderLine.backgroundColor = textField.text?.count == 0 ?  UIColor.lightGray :  UIColor.MHGold
        }
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let Limit = 10
        let currentLength:Int = (textField.text?.count)! + string.count - range.length
        let status = currentLength <= Limit
        return status
    }
}

extension MHLoginVC:countryCodeSelectorDelegate{
    func didSelectCountryCode(CountrycodeData: CountryCodeModel) {
        self.CountryCodeLb.text = "\(CountrycodeData.code!)"
        self.mobcode = CountrycodeData.value
    }
    
    
    func getCommonData(){
        
//        Webservice(url: "", parameters: ["action":"CommonData"]).executePOSTWithoutLoading(shouldReturn: .json) { (json, didComplete, response) in
//            if didComplete{
//                if let countryData = json as? [String:Any]{
//                    if let Country = countryData["country"] as? [[String:Any]]{
//                        for code in Country{
//                            self.countryCodeArray.append(CountryCodeModel(fromData: code) )
//                        }
//                    }
//                }
//            }
//        }
        
        
//        let parameters = ["action":"CommonData",
//                          "deviceid": Device_token,
//                          "devicetype": 2,
//                          "firebaseRegID": "1234"] as [String : Any]
        
        

        
        let parameters = ["action":"CommonData",
                          "guest_id": guest_id] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let countryData = response["data"] as? [String: Any]{
                    if let Country = countryData["country"] as? [[String:Any]]{
                        for code in Country{
                            self.countryCodeArray.append(CountryCodeModel(fromData: code) )
                        }
                    }
                }
                LoginCountryArray = self.countryCodeArray
//                let Country = UserDefaults.standard.value(forKey: "CountryName") as? String
                for item in self.countryCodeArray{
                    if item.selected == 1{
                        self.CountryCodeLb.text = item.code
                        self.mobcode = item.value
                    }
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                DispatchQueue.main.async {
                    self.CountryCodeLb.text = "+91"
                }
                //Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                
            }
        }
    }
    
    func guestlogin(){
        let parameters = [  "action" : "guestlogin",
                            "countrycode" : self.CountryCode,
                            "countryname" : self.CountryName] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let guestData = response["data"] as? [String:Any]{
                    self.GuestData = guestModel(fromData: guestData)
                }
                self.guest_id = self.GuestData.guestid
                UserDefaults.standard.set(self.GuestData.guestid, forKey: "guestId")
                isLanguageUpdated = true
                self.navigationController?.popViewController(animated: true)
            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                
            }
        }
    }
    
    func Login(){
        let parameters = ["action":"checkmobilenumber",
                          "mobcode": self.mobcode ?? "+91",
                          "mobile": PhoneTF.text!,
                          "deviceid": Device_token,
                          "devicetype": 2,
                          "firebaseRegID": "1234"] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                print("success")
                print(response["data"]!)
                if let data = response["data"] as? Json{
                    self.loginData = LoginModel(fromData: data)
                    print(self.loginData.otp)
                    let otpVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHOTPVC") as! MHOTPVC
                    otpVC.otp = self.loginData.otp!
                    otpVC.OTPmessage = self.loginData.message!
                    otpVC.Countrycode = self.CountryCodeLb.text
                    otpVC.code = self.mobcode
                    otpVC.PhoneNumber = self.PhoneTF.text!
                    otpVC.isFromLogin = true
                    self.navigationController?.pushViewController(otpVC, animated: true)
                }
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
}
extension UIScrollView {
   func scrollToBottom(animated: Bool) {
     if self.contentSize.height < self.bounds.size.height { return }
     let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
     self.setContentOffset(bottomOffset, animated: animated)
  }
}
