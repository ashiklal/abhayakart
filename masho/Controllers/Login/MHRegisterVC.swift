//
//  MHRegisterVC.swift
//  masho
//
//  Created by Appzoc on 26/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import KVSpinnerView
import IQKeyboardManagerSwift
var LoginCountryArray = [CountryCodeModel]()
class MHRegisterVC: UIViewController {

    @IBOutlet weak var NameTF: SkyFloatingLabelTextField!
    @IBOutlet weak var PhoneTF: UITextField!
    @IBOutlet weak var CountryCode: UILabel!
    @IBOutlet weak var CheckImageView: UIImageView!
    @IBOutlet weak var MobilePlaceholder: UILabel!
    @IBOutlet weak var Mobileunderline: UIView!
    @IBOutlet weak var scroll: UIScrollView!
    let Device_token : String = UserDefaults.standard.value(forKey: "Device_Token") as? String ?? "123456"
    var guest_id : Int = UserDefaults.standard.value(forKey: "guestId") as? Int ?? 0
    var countryCodeArray = [CountryCodeModel]()
    var isAccepted = false
    var RegisterData : LoginModel!
    var mobcode : String!
    var countryCode : String = "IN"
    var CountryName : String = "India"
    let locale = Locale.current
    var GuestData : guestModel! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        scroll.scrollToBottom(animated: false)
        scroll.bounces = false
        NameTF.textContentType = .oneTimeCode
        PhoneTF.textContentType = .oneTimeCode
        NameTF.titleFormatter = {$0 }
        NameTF.delegate = self
        PhoneTF.delegate = self
        NameTF.autocorrectionType = .no
        NameTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked(_:)))
        PhoneTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked(_:)))
//        let Country = UserDefaults.standard.value(forKey: "CountryName") as? String
//        if self.countryCodeArray != nil || self.countryCodeArray.count != 0{
//            for item in self.countryCodeArray{
//                if Country == item.name{
//                    self.CountryCode.text = item.code
//                    self.mobcode = item.value
//                }
//            }
//        }else{
            self.getCommonData()
//        }
        
//        CountryCode.text = countryCodeArray[0].code
//        mobcode = countryCodeArray[0].value
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        NameTF.resignFirstResponder()
        PhoneTF.resignFirstResponder()
    }
    @objc func doneButtonClicked(_ sender: Any) {
        NameTF.resignFirstResponder()
        PhoneTF.resignFirstResponder()
        var f = self.view.frame
        f.origin.y = 0
        self.view.frame = f
        MobilePlaceholder.textColor = UIColor.lightGray
        Mobileunderline.backgroundColor = UIColor.lightGray
    }
    @IBAction func BackBTNtapped(_ sender: Any) {
        
        if frompush {
            let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
             self.navigationController?.pushViewController(dashBoardVC, animated: true)
        }
        
        else {
        self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    
    @IBAction func SkipregistrationBTNtapped(_ sender: Any) {
        if guest_id == 0 {
//            self.ContryCode = locale.regionCode!
//            self.CountryName = countryName(from: locale.regionCode!)
            self.guestlogin()
        }else{
            isFromCartCheckout = false
            addReviewBeforeLogin = false
            ViewReviewBeforeLogin = false
            isLogged = false
            let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
            self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        }
        
//        let navigationStack = navigationController?.viewControllers ?? []
//        for vc in navigationStack{
//            if let dashboardVC = vc as? MHdashBoardVC{
//                DispatchQueue.main.async {
//                    self.navigationController?.popToViewController(dashboardVC, animated: true)
//                }
//            }
//        }
    
    }
    func countryName(from countryCode: String) -> String {
        if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return countryCode
        }
    }
    @IBAction func AcceptTermsAndConditions(_ sender: Any) {
        if isAccepted{
            isAccepted = false
            CheckImageView.image = UIImage(named: "Check")
        }else{
            isAccepted = true
            CheckImageView.image = UIImage(named: "Checked")
        }
    }
    
    @IBAction func CountryCode(_ sender: Any) {
        NameTF.resignFirstResponder()
        PhoneTF.resignFirstResponder()
        let codeVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCountryCodeVC") as! MHCountryCodeVC
        codeVC.countryCodeArray = self.countryCodeArray
        codeVC.screenShot = self.view.takeScreenshot()
        codeVC.delegate = self
        self.navigationController?.pushViewController(codeVC, animated: false)
        
    }
    @IBAction func TermsAndConditions(_ sender: Any) {
        NameTF.resignFirstResponder()
        PhoneTF.resignFirstResponder()
        let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
        WebViewVC.WebURL = "https://www.masho.com/termsandcondition"
        self.navigationController!.pushViewController(WebViewVC, animated: true)
        
    }
    @IBAction func RegisterBTNtapped(_ sender: Any) {
        NameTF.resignFirstResponder()
        PhoneTF.resignFirstResponder()
        if NameTF.text == ""{
            Banner.main.showBanner(title: "", subtitle: "Name field is mandatory", style: .danger)
        }else if PhoneTF.text == ""{
            Banner.main.showBanner(title: "", subtitle: "Mobile number field is mandatory", style: .danger)
        }else{
            let valid_name1 = BaseValidator.containsDigits(string: NameTF.text)
            let valid_name2 = BaseValidator.isNotcontainsSpecialCharacters(string: NameTF.text)
            if valid_name1 == false && valid_name2{
                if isAccepted{
                    Register()
                }else{
                    Banner.main.showBanner(title: "", subtitle: "Please agree to the terms and conditions", style: .danger)
                }
//                let DashboardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
//                self.navigationController?.pushViewController(DashboardVC, animated: true)
            }else{
                Banner.main.showBanner(title: "", subtitle: "Avoid digits and special characters from name field", style: .danger)
            }
        }
    }
    
    
}
extension MHRegisterVC: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        if textField == PhoneTF{
            MobilePlaceholder.textColor = UIColor.MHGold
            Mobileunderline.backgroundColor = UIColor.MHGold
            var f = self.view.frame
//        if keyboardHeight == 0{
            f.origin.y = -250
//        }else{
//            f.origin.y = -keyboardHeight
//        }
            self.view.frame = f
//        }
//        if textField == NameTF{
//            MobilePlaceholder.textColor = UIColor.lightGray
//            Mobileunderline.backgroundColor = UIColor.lightGray
//            var f = self.view.frame
//            f.origin.y = -240
//            self.view.frame = f
//        }
        return true
    }
//    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
//        if textField == PhoneTF{
//            MobilePlaceholder.textColor = UIColor.lightGray
//            Mobileunderline.backgroundColor = UIColor.lightGray
//            var f = self.view.frame
//            f.origin.y = 0
//            self.view.frame = f
//        }else{
//            var f = self.view.frame
//            f.origin.y = 0
//            self.view.frame = f
//        }
//        return true
//    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == PhoneTF{
            let Limit = 10
            let currentLength:Int = (textField.text?.count)! + string.count - range.length
            let status = currentLength <= Limit
            return status
        }
        return true
    }
}
extension MHRegisterVC: countryCodeSelectorDelegate{
    func didSelectCountryCode(CountrycodeData: CountryCodeModel) {
        self.CountryCode.text = "\(CountrycodeData.code!)"
        self.mobcode = CountrycodeData.value
    }
    
    func Register(){
        let parameters = ["action":"checkregistartion",
                          "mobcode":  mobcode!,
                          "mobile": self.PhoneTF.text!,
                          "name": self.NameTF.text!,
                          "deviceid": Device_token,
                          "devicetype": 2,
                          "firebaseRegID": "1234"] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                if let data = response["data"] as? Json{
                    self.RegisterData = LoginModel(fromData: data)
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                let otpVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHOTPVC") as! MHOTPVC
                otpVC.otp = self.RegisterData.otp!
                otpVC.OTPmessage = self.RegisterData.message!
                otpVC.code = self.mobcode
                otpVC.PhoneNumber = self.PhoneTF.text!
                otpVC.isFromLogin = false
                self.navigationController?.pushViewController(otpVC, animated: true)
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
            }
        }
    }
    func guestlogin(){
        let parameters = [  "action" : "guestlogin",
                            "countrycode" : self.countryCode,
                            "countryname" : self.CountryName] as [String : Any]
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let guestData = response["data"] as? [String:Any]{
                    self.GuestData = guestModel(fromData: guestData)
                }
                self.guest_id = self.GuestData.guestid
                UserDefaults.standard.set(self.GuestData.guestid, forKey: "guestId")
                isLanguageUpdated = true
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)            case .failure :
                print("failure")
                Banner.main.showBanner(title: "", subtitle: "\(response["message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                
            }
        }
    }
    
    
    func getCommonData(){

        let parameters = ["action":"CommonData",
                          "guest_id": guest_id] as [String : Any]
        self.view.alpha = 0.5
        self.view.isUserInteractionEnabled = false
        KVSpinnerView.show()
        NetworkManager.webcallWithErrorCode(urlString: baseURL, methodeType: .post,parameter: parameters) { (status, response) in
            print(response)
            switch status {
            case .noNetwork:
                print("network error")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "You seem to have a bad network connection", style: .danger)
            case .success :
                print("success")
                print(response["data"]!)
                if let countryData = response["data"] as? [String: Any]{
                    if let Country = countryData["country"] as? [[String:Any]]{
                        for code in Country{
                            self.countryCodeArray.append(CountryCodeModel(fromData: code) )
                        }
                    }
                }
                for item in self.countryCodeArray{
                    if item.selected == 1{
                        self.CountryCode.text = item.code
                        self.mobcode = item.value
                    }
                }
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                
            case .failure :
                print("failure")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                Banner.main.showBanner(title: "", subtitle: "\(response["Message"]!)", style: .danger)
            case .unknown:
                print("unknown")
                self.view.alpha = 1
                self.view.isUserInteractionEnabled = true
                KVSpinnerView.dismiss()
                DispatchQueue.main.async {
                    self.CountryCode.text = "+91"
                }
                //Banner.main.showBanner(title: "", subtitle: "Oops! Something went wrong", style: .danger)
                
            }
        }
    }
    
}
