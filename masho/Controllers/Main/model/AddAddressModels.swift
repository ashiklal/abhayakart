//
//  AddAddressModels.swift
//  masho
//
//  Created by Castler on 28/10/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
class MHaddNewAddressModel{
    var btndata :  MHAddAddress_btndataModel!
    var data = [MHAddAddressModel]()
    var errorcode : String!
    var geolocation : Int!
    var message : String!
    init(fromData data: [String:Any]) {
        self.errorcode = data["errorcode"] as? String
        self.geolocation = data["geolocation"] as? Int
        self.message = data["message"] as? String
        if let data = data["data"] as? [[String: Any]]{
            for item in data{
                self.data.append(MHAddAddressModel(fromData: item))
            }
        }
        if let btndatas = data["btndata"] as? [String:Any]{
            self.btndata = MHAddAddress_btndataModel(fromData: btndatas)
        }
    }
}
class MHAddAddress_btndataModel{
    var apiname : String!
    var btndata : MHAddAddress_btnColorModel!
    var fieldname : String!
    var fieldtype : String!
    var label : String!
    var placeholder : String!
    var required : String!
    var size : String!
    var value : String!
    init(fromData data: [String:Any]) {
        self.apiname = data["apiname"] as? String
        self.fieldtype = data["fieldtype"] as? String
        self.label = data["label"] as? String
        self.placeholder = data["placeholder"] as? String
        self.fieldname = data["fieldname"] as? String
        self.size = data["size"] as? String
        self.value = data["value"] as? String
        if let btndatas = data["btndata"] as? [String:Any]{
            self.btndata = MHAddAddress_btnColorModel(fromData: btndatas)
        }
    }
}
class MHAddAddress_btnColorModel{
    var background : String!
    var color : String!
    init(fromData data: [String:Any]) {
        self.background = data["background"] as? String
        self.color = data["color"] as? String
    }
}
class MHAddAddressModel{
    var fieldname : String!
    var fieldtype : String!
    var label : String!
    var placeholder : String!
    var required : Int!
    var size : String!
    var value : String!
    var arrayValue = [String]()
    var options = [MHAddAddressOptionsModel]()
    var btndata : MHAddAddress_btnColorModel!
    init(fromData data: [String:Any]) {
        self.fieldname = data["fieldname"] as? String
        self.fieldtype = data["fieldtype"] as? String
        self.label = data["label"] as? String
        self.placeholder = data["placeholder"] as? String
        self.required = data["required"] as? Int
        self.size = data["size"] as? String
        self.value = data["value"] as? String
        if let arrayData = data["options"] as? [String]{
            for item in arrayData{
                self.arrayValue.append(item)
            }
        }
        if let optionsData = data["options"] as? [[String:Any]]{
            for item in optionsData{
                self.options.append(MHAddAddressOptionsModel(fromData: item))
            }
        }
        if let btndatas = data["btndata"] as? [String:Any]{
            self.btndata = MHAddAddress_btnColorModel(fromData: btndatas)
        }
    }
}
class MHAddAddressOptionsModel{
    var countryname : String!
    var name : String!
    var value : String!
    var image : String!
    init(fromData data: [String:Any]) {
        self.countryname = data["countryname"] as? String
        self.name = data["name"] as? String
        self.value = data["value"] as? String
        self.image = data["image"] as? String
    }
}
class stateListModel{
    var fieldname : String!
    var fieldtype : String!
    var label : String!
    var options = [stateListOptionsModel]()
    var placeholder : String!
    var required : String!
    var size : String!
    var value = [String]()
    init(fromData data: [String:Any]) {
        self.fieldname = data["fieldname"] as? String
        self.fieldtype = data["fieldtype"] as? String
        self.label = data["label"] as? String
        if let options = data["options"] as? [[String:Any]]{
            for state in options{
                self.options.append(stateListOptionsModel(fromData: state))
            }
        }
        self.placeholder = data["placeholder"] as? String
        self.required = data["required"] as? String
        self.size = data["size"] as? String
        if let options = data["value"] as? [String]{
            self.value = options
        }
    }
}
class stateListOptionsModel{
    var name : String!
    var value : String!
    init(fromData data: [String:Any]) {
        self.name = data["name"] as? String
        self.value = data["value"] as? String
    }
}
