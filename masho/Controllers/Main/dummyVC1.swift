//
//  dummyVC1.swift
//  masho
//
//  Created by Appzoc on 22/09/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

class dummyVC1: UIViewController {
    let basevc = MHBaseViewVC()
    var delegate : displayNewVCdelegate! = nil
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func ClickHereBTNtapped(_ sender: UIButton) {
        let vc = StoryBoard.main.instantiateViewController(withIdentifier: "dummyVC2") as! dummyVC2
        delegate.displayNewVC(VC: vc)
    }
}
