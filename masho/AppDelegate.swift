//
//  AppDelegate.swift
//  masho
//
//  Created by Appzoc on 25/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import KVSpinnerView
import Firebase
import FirebaseDynamicLinks
import UserNotifications
import GoogleMaps
import GooglePlaces
import AVFoundation
var devicetoken = ""
var frompush = false
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    let navigator = Navigator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        
        IQKeyboardManager.shared.enable = true
//        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.toolbarTintColor = UIColor.black
        
        KVSpinnerView.settings.animationStyle = KVSpinnerViewSettings.AnimationStyle.infinite
        KVSpinnerView.settings.linesCount = 3
        KVSpinnerView.settings.backgroundRectColor = .clear
        KVSpinnerView.settings.tintColor = UIColor.MHGold
        
        GMSServices.provideAPIKey("AIzaSyDX6IRIZeq7YKttAjQDExw23qxfgvgrkrg")
        GMSPlacesClient.provideAPIKey("AIzaSyDX6IRIZeq7YKttAjQDExw23qxfgvgrkrg")
//        GMSServices.provideAPIKey("AIzaSyBxuGQu5WGqBdjNzUmDS86xVaZ0gSyYj9c")
//        GMSPlacesClient.provideAPIKey("AIzaSyBxuGQu5WGqBdjNzUmDS86xVaZ0gSyYj9c")
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .moviePlayback)
        }
        catch {
            print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
        registerForPushNotifications()
  
//        let isLoggedIn = UserDefaults.standard.object(forKey: "isLoggedIn") as? Bool ?? false
//        if isLoggedIn == true{
//            self.window = UIWindow(frame: UIScreen.main.bounds)
//            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "MHdashBoardVC")
//            let navigationController = UINavigationController(rootViewController: vc)
//            navigationController.setNavigationBarHidden(true, animated: false)
//            window?.rootViewController = navigationController
//            window?.makeKeyAndVisible()
//        }else{
//            let deviceid = "kjhbuhiuhiubijbbbobhouhyb"
//            UserDefaults.standard.set(deviceid, forKey: "Device_Token")
//            
//            UserDefaults.standard.set("", forKey: "userID")
//            UserDefaults.standard.set("", forKey: "tableID")
//            UserDefaults.standard.set("", forKey: "userName")
//
//            
//            self.window = UIWindow(frame: UIScreen.main.bounds)
//            let vc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "MHLoginVC")
//            let navigationController = UINavigationController(rootViewController: vc)
//            navigationController.setNavigationBarHidden(true, animated: false)
//            window?.rootViewController = navigationController
//            window?.makeKeyAndVisible()
//        }
        
        return true
    }
//    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
//        guard let url = userActivity.webpageURL else { return false}
//        guard let vc = navigator.getDestination(for: url) else {
//            application.open(url)
//            return false
//        }
//        window?.rootViewController = vc
//        window?.makeKeyAndVisible()
//        return true
//    }
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current() // 1
            .requestAuthorization(options: [.alert, .sound, .badge]) { // 2
                granted, error in
                print("Permission granted: \(granted)") // 3
                guard granted else { return }
                self.getNotificationSettings()
        }
    }
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
       {
           completionHandler([.alert,.badge,.sound])

           if let info = notification.request.content.userInfo as? [String:AnyObject] {
               print("info app in forground notification :\(info)")

               if let _ = info["aps"] as? [String:AnyObject] {



               }
           }
       }

    
    
    func application(_ application: UIApplication,didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data in String(format: "%02.2hhx", data) }
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        devicetoken = token
        UserDefaults.standard.set(token, forKey: "Device_Token")
    }
    
    func application(_ application: UIApplication,didFailToRegisterForRemoteNotificationsWithError error: Error) {
        devicetoken = "No Token"
        print("Failed to register for remote notifications with error: \(error)")
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("Push notification received:")
        print("User Info ",userInfo)
        if ( UIApplication.shared.applicationState == UIApplication.State.inactive) || ( UIApplication.shared.applicationState == UIApplication.State.background) || ( UIApplication.shared.applicationState == UIApplication.State.active){
            
            
            if let user_Info = userInfo["aps"] as? [String: AnyObject] {
                let urlType = userInfo["urltye"] as? String
                if urlType == "1" {
                    let urlaction = userInfo["urlaction"] as? String
                    switch urlaction{
                    case "1":
                        //dashboard
                        let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
                        let navigationVC = UINavigationController(rootViewController: dashBoardVC)
                        navigationVC.navigationBar.isHidden = true
                        self.window?.rootViewController = navigationVC
                        self.window?.makeKeyAndVisible()
                        
                    case "2":
                        //category details
                        let categoryDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCategoryDetailsVC") as! MHCategoryDetailsVC
                        categoryDetailsVC.api_Key = Int((userInfo["extradata"] as? String)!)
                        let extradata = user_Info["extradata"] as? String
                        if extradata == "147"{
                            categoryDetailsVC.nav_Title = "Abayas/Burqas"
                        }else if extradata == "145"{
                            categoryDetailsVC.nav_Title = "Hijabs/Scarves/Wraps"
                        }else if extradata == "5" {
                            categoryDetailsVC.nav_Title = "Full Sleeve Kurtis"
                        }else if extradata == "51"{
                            categoryDetailsVC.nav_Title = "Bags"
                        }else{
                            categoryDetailsVC.nav_Title = ""
                        }
                        let navigationVC = UINavigationController(rootViewController: categoryDetailsVC)
                        navigationVC.navigationBar.isHidden = true
                        
                        self.window?.rootViewController = navigationVC
                        
                        self.window?.makeKeyAndVisible()
                    case "3":
                        //product details
                        let destinationVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
                        let navigationVC = UINavigationController(rootViewController: destinationVC)
                        navigationVC.navigationBar.isHidden = true
                        destinationVC.isFrom = .other
                      productID = userInfo["extradata"] as? String ?? ""
                        self.window?.rootViewController = navigationVC
                       
                        self.window?.makeKeyAndVisible()
                    case "4":
                        //cart
                        let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                        let navigationVC = UINavigationController(rootViewController: CartVC)
                        navigationVC.navigationBar.isHidden = true
                        self.window?.rootViewController = navigationVC
                        
                        self.window?.makeKeyAndVisible()
                    case "5":
                        //wishlist
                        let wishlistVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHWishListVC") as! MHWishListVC
                        let navigationVC = UINavigationController(rootViewController: wishlistVC)
                        navigationVC.navigationBar.isHidden = true
                       
                        self.window?.rootViewController = navigationVC
                        self.window?.makeKeyAndVisible()
                    case "6":
                        //recent list
                        let RescentProductListVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHRescentProductListVC") as! MHRescentProductListVC
                        let navigationVC = UINavigationController(rootViewController: RescentProductListVC)
                        navigationVC.navigationBar.isHidden = true
                        self.window?.rootViewController = navigationVC
                        
                        self.window?.makeKeyAndVisible()
                    case "7":
                        //login
                        let loginvc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "MHLoginVC")
                        let navigationVC = UINavigationController(rootViewController: loginvc)
                        navigationVC.navigationBar.isHidden = true
                        self.window?.rootViewController = navigationVC
                        
                        self.window?.makeKeyAndVisible()
                    case "8":
                        //registration
                        let RegVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHRegisterVC") as! MHRegisterVC
                        let navigationVC = UINavigationController(rootViewController: RegVC)
                        navigationVC.navigationBar.isHidden = true
                        self.window?.rootViewController = navigationVC
                        
                        self.window?.makeKeyAndVisible()
                    default:
                        break
                    }
                    
                }
                
                else{
                    let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                    WebViewVC.WebURL = userInfo["urlaction"] as? String ??  ""
                    let navigationVC = UINavigationController(rootViewController: WebViewVC)
                    navigationVC.navigationBar.isHidden = true
                    self.window?.rootViewController = navigationVC
                    
                    self.window?.makeKeyAndVisible()
                }
            }
            
            
            
            
        }
        
    }
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,didReceive response: UNNotificationResponse,withCompletionHandler completionHandler: @escaping () -> Void) {
        print("tapped in info :\(response.notification.request.content.userInfo)")
        guard let userInfo = response.notification.request.content.userInfo as? [String:AnyObject] else {  return }
        guard let aps = userInfo["aps"] as? [String:AnyObject] else { return }
        if ( UIApplication.shared.applicationState == UIApplication.State.inactive) || ( UIApplication.shared.applicationState == UIApplication.State.background) || ( UIApplication.shared.applicationState == UIApplication.State.active){
            
            
            if let user_Info = userInfo["aps"] as? [String: AnyObject] {
                let urlType = userInfo["urltye"] as? String
                if urlType == "1"{
                    let urlaction = userInfo["urlaction"] as? String
                    switch urlaction{
                    case "1":
                        //dashboard
                        let dashBoardVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHdashBoardVC") as! MHdashBoardVC
                        let navigationVC = UINavigationController(rootViewController: dashBoardVC)
                        navigationVC.navigationBar.isHidden = true
                        self.window?.rootViewController = navigationVC
                        self.window?.makeKeyAndVisible()
                        print("//dashboard")
                    case "2":
                        //category details
                        let categoryDetailsVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCategoryDetailsVC") as! MHCategoryDetailsVC
                        categoryDetailsVC.api_Key = Int((userInfo["extradata"] as? String)!)
                        let extradata = user_Info["extradata"] as? String
                        if extradata == "147"{
                            categoryDetailsVC.nav_Title = "Abayas/Burqas"
                        }else if extradata == "145"{
                            categoryDetailsVC.nav_Title = "Hijabs/Scarves/Wraps"
                        }else if extradata == "5" {
                            categoryDetailsVC.nav_Title = "Full Sleeve Kurtis"
                        }else if extradata == "51"{
                            categoryDetailsVC.nav_Title = "Bags"
                        }else{
                            categoryDetailsVC.nav_Title = ""
                        }
                        let navigationVC = UINavigationController(rootViewController: categoryDetailsVC)
                        navigationVC.navigationBar.isHidden = true
                        frompush = true
                        self.window?.rootViewController = navigationVC
                        self.window?.makeKeyAndVisible()
                    case "3":
                        //product details
                        let destinationVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
                        let navigationVC = UINavigationController(rootViewController: destinationVC)
                        navigationVC.navigationBar.isHidden = true
                        frompush = true
                        destinationVC.isFrom = .other
                       productID = userInfo["extradata"] as? String ?? ""
                        self.window?.rootViewController = navigationVC
                        self.window?.makeKeyAndVisible()
                    case "4":
                        //cart
                        let CartVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHCartVC") as! MHCartVC
                        let navigationVC = UINavigationController(rootViewController: CartVC)
                        navigationVC.navigationBar.isHidden = true
                        frompush = true
                        self.window?.rootViewController = navigationVC
                        self.window?.makeKeyAndVisible()
                    case "5":
                        //wishlist
                        let wishlistVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHWishListVC") as! MHWishListVC
                        let navigationVC = UINavigationController(rootViewController: wishlistVC)
                        navigationVC.navigationBar.isHidden = true
                        frompush = true
                        self.window?.rootViewController = navigationVC
                        self.window?.makeKeyAndVisible()
                    case "6":
                        //recent list
                        let RescentProductListVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHRescentProductListVC") as! MHRescentProductListVC
                        let navigationVC = UINavigationController(rootViewController: RescentProductListVC)
                        navigationVC.navigationBar.isHidden = true
                        frompush = true
                        self.window?.rootViewController = navigationVC
                        self.window?.makeKeyAndVisible()
                    case "7":
                        //login
                        let loginvc = UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "MHLoginVC") 
                        let navigationVC = UINavigationController(rootViewController: loginvc)
                        navigationVC.navigationBar.isHidden = true
                        self.window?.rootViewController = navigationVC
                        self.window?.makeKeyAndVisible()
                    case "8":
                        //registration
                        let RegVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHRegisterVC") as! MHRegisterVC
                        let navigationVC = UINavigationController(rootViewController: RegVC)
                        navigationVC.navigationBar.isHidden = true
                        self.window?.rootViewController = navigationVC
                        self.window?.makeKeyAndVisible()
                    default:
                        break
                    }
                }
                
                else{
                    let WebViewVC = StoryBoard.login.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                    WebViewVC.WebURL = userInfo["urlaction"] as? String ??  ""
                    let navigationVC = UINavigationController(rootViewController: WebViewVC)
                    navigationVC.navigationBar.isHidden = true
                    self.window?.rootViewController = navigationVC
                    self.window?.makeKeyAndVisible()
                }
            }
            
            
            
        }
    }
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL {
            print("Incoming URL is :\(incomingURL)")
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
                guard error == nil else {
                    print("Found an error :\(String(describing: error?.localizedDescription))")
                    return
                }
                if let dynamicLink = dynamicLink {
                    self.handleIncomingDynamicLink(dynamicLink)
                }
            }
            if linkHandled {
                return true
            } else {
                return false
                // Handle Other cases
            }
        }
        return false
    }
    
    fileprivate func handleIncomingDynamicLink(_ dynamicLink:DynamicLink) {
        guard let url = dynamicLink.url else {
            print("link Object have no url")
            return
        }
        print("Incoming link parameter is :\(url.absoluteString)")
        var info: [String: String] = [:]
        
        URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
            info[$0.name] = $0.value
        }
        
        guard dynamicLink.matchType == .unique || dynamicLink.matchType == .default else {return}
        redirect(withInfo: info)
    }
    
    fileprivate func redirect(withInfo info:[String:Any]) {
        if let value = info.first?.value {
            let product_id = (value as AnyObject).description
            let destinationVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
            let navigationVC = UINavigationController(rootViewController: destinationVC)
            navigationVC.navigationBar.isHidden = true
            destinationVC.isFrom = .other
            productID = product_id ?? ""
            self.window?.rootViewController = navigationVC
            self.window?.makeKeyAndVisible()
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        self.window?.endEditing(true)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "masho")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

