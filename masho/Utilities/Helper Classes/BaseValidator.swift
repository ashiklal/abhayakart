//
//  BaseValidator.swift
//  Follow
//
//  Created by Appzoc on 10/01/18.
//  Copyright © 2018 appzoc. All rights reserved.
//

import Foundation
import UIKit

public class BaseValidator {
    
    fileprivate init(){}
    
//    class func unwrap<T>(element: T?) -> T {
//        guard let nonEmptyValue = element else {
//            print("TYpeee",element)
//            if element is String?{
//                return "" as! T
//            }else if element is Double?{
//                return 0.0 as! T
//            }else{
//                print("elsecase")
//                return 0.0 as! T
//            }
//
//        }
//
//        return nonEmptyValue
//    }
    
    class func unwrap(string: String?)-> String{
        guard let nonEmptyValue = string else {
            return ""
        }
        return nonEmptyValue
    }
    
    
    class func unwrap(integer: Int?)-> Int{
        guard let nonEmptyValue = integer else {
            return 0
        }
        return nonEmptyValue
    }
    
    
    class func unwrap(double: Double?)-> Double{
        guard let nonEmptyValue = double else {
            return 0.0
        }
        return nonEmptyValue
    }
    
    
    
    
    class func isValid(email: String?) -> Bool {
        guard let email = email, email != "" else { return false }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTester = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTester.evaluate(with: email)
    }
    
    
    class func isValid(phone: String?)->Bool {
        guard let phone = phone, phone != "" else { return false }
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = phone.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        
        if phone == filtered {
            let phoneRegex = "[235689][0-9]{6}([0-9]{3})?"
            let phoneNumberTester = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  phoneNumberTester.evaluate(with: phone)
        }else {
            return false
        }
    }
    
    class func isValid(digits: String)->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = digits.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  digits == filtered
    }
    
    class func isValid(number: String?) -> Bool {
        guard let num = number,number != nil else {
            return false
        }
        let characters = CharacterSet.decimalDigits.inverted
        return !num.isEmpty && num.rangeOfCharacter(from: characters) == nil
    }
    
    class func isNotEmpty(string: String?) -> Bool
    {
        guard let text = string else { print("Base validator , found nil value");return false }
        guard !(text.trimmingCharacters(in: .whitespaces).isEmpty), string != "" else {
            return false
        }
        return true
    }
    
    class func invalidStringCheck(withTextField: UITextField, assigningString: String){
        DispatchQueue.main.async {
            withTextField.textColor = UIColor.gray
            withTextField.text = nil
            if !assigningString.trimmingCharacters(in: .whitespaces).isEmpty{
                withTextField.text = assigningString.trimmingCharacters(in: .whitespaces)
            }
        }
        
    }
    
    class func isNotcontainsSpecialCharacters(string:String?) -> Bool {
        guard let text = string , text != "" else {return false}
//        let regex = ".*[^A-Za-z0-9].*"
//        let testString = NSPredicate(format:"SELF MATCHES %@", regex)
//        return testString.evaluate(with: string)
        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 ") //special Chara only
        if text.rangeOfCharacter(from: characterset.inverted) != nil {
            print("string contains special characters")
            return false
        }
        return true
    }
    
    class func containsDigits(string:String?) -> Bool{
         guard let text = string , text != "" else {return false}
        let characterset = CharacterSet.decimalDigits
        return text.rangeOfCharacter(from: characterset) != nil
    }
    
   class func passWordregex(string:String) -> Bool{
        //(?=.*[a-z])
        if string.rangeOfCharacter(from: .whitespaces) != nil {
            //BannerClass.main.showBanner(title: "", subtitle: "spaces are not allowed", style: .info)
            return false
        }
        let regex = "^(?=.*[A-Z])(?=.*\\d)(?=.*[d$@$!%*?&#])[A-Za-z\\dd$@$!%*?&#]{4,}"
        let testString = NSPredicate(format:"SELF MATCHES %@", regex)
        return testString.evaluate(with: string)
    }
}

