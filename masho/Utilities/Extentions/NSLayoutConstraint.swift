//
//  NSLayoutConstraint.swift
//  DayToFresh
//
//  Created by farhan k on 10/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit


extension NSLayoutConstraint {
    override open var description: String {
        let id = identifier ?? ""
        return "id: \(id), constant: \(constant)" //you may print whatever you want here
    }
}
